describe("Settings", function() {
  var settings = myWF.settings;

  it("has a temperature setting", function() {
        expect(settings.temperatureMeasureUnit).toBeDefined();
  });

  it("converts temperatures", function() {
        settings.temperatureMeasureUnit = 'F';
        expect(settings.convertTemperature(10)).toEqual((90/5)+32);
  });

  it("has a wind setting", function() {
	    expect(settings.windMeasureUnit).toBeDefined();
  });

  it("can be saved", function() {
	    expect(settings.save).toBeDefined();
	    expect(settings.load).toBeDefined();
	    settings.windMeasureUnit = myWF.Wind.KNOTS;
	    settings.temperatureMeasureUnit = 'F';
	    settings.save();
	    settings.windMeasureUnit = '';
	    settings.temperatureMeasureUnit = '';
		settings.load();
		expect(settings.windMeasureUnit).toEqual(myWF.Wind.KNOTS);
	    expect(settings.temperatureMeasureUnit).toEqual('F');
  });

});