describe("LocationList_YAHOO-Specs", function() {
    var forecastList = null;

    beforeEach(function() {
        ajaxList = myWF.createGeoAjaxList(myWF.createLocationList_YAHOO());
    });

    it("Gets an URL by its service", function() {
    	if (myWF.isLocal) {
	        var url = 'http://localhost:8082/whereyahoo/geocode?location=Adro,Italy&locale=' +
	               myWF.util.getLocale() + '&flags=J&appid=' + myWF.keys.YAHOO_appid;
    	} else {
	        var url = 'http://where.yahooapis.com/geocode?location=Adro,Italy&locale=' +
	               myWF.util.getLocale() + '&flags=J&appid=' + myWF.keys.YAHOO_appid;
    	}
        expect(ajaxList.serviceAdapter.getURL('Adro,Italy')).toEqual(url);
    });
    
    it("Put an error for empty data by its service", function() {
        var error = {};
        var list = ajaxList.serviceAdapter.convertHead(testEmptyData, null, error);
        expect(error.code).not.toEqual('');
        expect(list).toEqual([]);
    });
    
    it("Converts to locations by its service", function() {
        var error = {};
        var service = ajaxList.serviceAdapter;
        var rawList = service.convertHead(testData, null, error);
        expect(!error['code']).toEqual(true);
        expect(rawList.length).toEqual(4);
        var list = myWF.util.map(rawList, function(e) {
            return service.convertElement(e);
        });
        expect(list.length).toEqual(4);
        expect(list[0].latitude).toEqual('35.330805');
        expect(list[0].countryCode).toEqual('US');
        expect(list[0].constructor).toBe(myWF.Location);
        expect(list[1].city).toEqual('London');
        expect(list[1].county).toEqual("Laurel County");
        expect(list[1].countyCode).toEqual('BS');
        expect(list[2].state).toEqual("Ohio");
        expect(list[2].country).toEqual('Stati Uniti');
        expect(list[3].longitude).toEqual('-99.564850');
        expect(list[3].neighborhood).toEqual('Frazione o altro');
    });
    
    it("Reports failed calls", function() {
        var status = ajaxList.status ;
        ajaxList.serviceAdapter.getURL = function () { return 'http://xxx.yyy.aaa'; };
        spyOn(ajaxList, 'onFail').andCallThrough();
        ajaxList.update("Venice,US");
        waitsFor(function() {
            return (status.isNotOk());
        }, "Never fired OnFail", 10000);
        runs(function() {
            expect(ajaxList.onFail).toHaveBeenCalled();
            //expect(status.updated).toEqual(false);
        });
    });
    
    it("Gets an ajax list of locations by name", function() {
        var status = ajaxList.status, 
            success = false;
        ajaxList.onSuccess = function () { 
            success = true; 
        };
        spyOn(ajaxList, 'onSuccess').andCallThrough();
        ajaxList.update("Venice, US");
        waitsFor(function() {
            return success;
        }, "Never got an ajax list of locations by name", 1000);
        runs(function() {
            expect(ajaxList.onSuccess).toHaveBeenCalled();
            expect(status.isOk()).toEqual(true);
            expect(ajaxList.data.list.length).not.toEqual(0);
            expect(ajaxList.data.list[0].latitude).not.toEqual(NaN);
            expect(ajaxList.data.list[0].constructor).toBe(myWF.Location);
        });
    });

    it("Gets an ajax list of locations by geo coordinates", function() {
        var status = ajaxList.status,
            success = false;
        ajaxList.onSuccess = function () { 
            success = true; 
        };
        spyOn(ajaxList, 'onSuccess').andCallThrough();
        var onFail = function () {};
		myWF.util.getGPSPosition(function (position) {
	        ajaxList.update(position.coords.latitude + '+' + position.coords.longitude + '&gflags=R');
	        	//'35.330805+-93.253879&gflags=R');
		}, onFail);
        //ajaxList.update('35.330805+-93.253879&gflags=R');
        waitsFor(function() {
            return success;
        }, "Never got an ajax list of locations by geo coordinates", 1000);
        runs(function() {
            expect(ajaxList.onSuccess).toHaveBeenCalled();
            expect(status.isOk()).toEqual(true);
            expect(ajaxList.data.list[0].city).not.toEqual('');
            expect(ajaxList.data.list[0].state).not.toEqual('');
        });
    });
});

testEmptyData = {
    "ResultSet": {
        "version": "1.0",
        "Error": 0,
        "ErrorMessage": "Nessun errore",
        "Locale": "it",
        "Quality": 10,
        "Found": 0
    }
};

OldTestData = {
    "ResultSet" : {
        "version" : "1.0",
        "Error" : 0,
        "ErrorMessage" : "Nessun errore",
        "Locale" : "it",
        "Quality" : 40,
        "Found" : 4,
        "Results" : [ {
            "quality" : 40,
            "latitude" : "35.330805",
            "longitude" : "-93.253879",
            "offsetlat" : "35.330805",
            "offsetlon" : "-93.253879",
            "radius" : 4500,
            "name" : "",
            "line1" : "",
            "line2" : "London, AR",
            "line3" : "",
            "line4" : "Stati Uniti",
            "house" : "",
            "street" : "",
            "xstreet" : "",
            "unittype" : "",
            "unit" : "",
            "postal" : "",
            "neighborhood" : "",
            "city" : "London",
            "county" : "Pope County",
            "state" : "Arkansas",
            "country" : "Stati Uniti",
            "countrycode" : "US",
            "statecode" : "AR",
            "countycode" : "",
            "uzip" : "72847",
            "hash" : "",
            "woeid" : 2441278,
            "woetype" : 7
        }, {
            "quality" : 40,
            "latitude" : "37.127175",
            "longitude" : "-84.083774",
            "offsetlat" : "37.127175",
            "offsetlon" : "-84.083774",
            "radius" : 4900,
            "name" : "",
            "line1" : "",
            "line2" : "London, KY",
            "line3" : "",
            "line4" : "Stati Uniti",
            "house" : "",
            "street" : "",
            "xstreet" : "",
            "unittype" : "",
            "unit" : "",
            "postal" : "",
            "neighborhood" : "",
            "city" : "London",
            "county" : "Laurel County",
            "state" : "Kentucky",
            "country" : "Stati Uniti",
            "countrycode" : "US",
            "statecode" : "KY",
            "countycode" : "BS",
            "uzip" : "40741",
            "hash" : "",
            "woeid" : 2441293,
            "woetype" : 7
        }, {
            "quality" : 40,
            "latitude" : "39.885964",
            "longitude" : "-83.448130",
            "offsetlat" : "39.885964",
            "offsetlon" : "-83.448130",
            "radius" : 4800,
            "name" : "",
            "line1" : "",
            "line2" : "London, OH",
            "line3" : "",
            "line4" : "Stati Uniti",
            "house" : "",
            "street" : "",
            "xstreet" : "",
            "unittype" : "",
            "unit" : "",
            "postal" : "",
            "neighborhood" : "",
            "city" : "London",
            "county" : "Madison County",
            "state" : "Ohio",
            "country" : "Stati Uniti",
            "countrycode" : "US",
            "statecode" : "OH",
            "countycode" : "",
            "uzip" : "43140",
            "hash" : "",
            "woeid" : 2441292,
            "woetype" : 7
        }, {
            "quality" : 40,
            "latitude" : "30.685777",
            "longitude" : "-99.564850",
            "offsetlat" : "30.685777",
            "offsetlon" : "-99.564850",
            "radius" : 1100,
            "name" : "",
            "line1" : "",
            "line2" : "London, TX",
            "line3" : "",
            "line4" : "Stati Uniti",
            "house" : "",
            "street" : "",
            "xstreet" : "",
            "unittype" : "",
            "unit" : "",
            "postal" : "",
            "neighborhood" : "Frazione o altro",
            "city" : "London",
            "county" : "Kimble County",
            "state" : "Texas",
            "country" : "Stati Uniti",
            "countrycode" : "US",
            "statecode" : "TX",
            "countycode" : "",
            "uzip" : "76854",
            "hash" : "",
            "woeid" : 2441291,
            "woetype" : 7
        } ]
    }
};


testData = { 
    "query" : { 
        "count" : 2,
        "created" : "2013-04-11T22:25:44Z",
        "lang" : "en-US",
        "results" : { 
            "Result" : [ { "city" : "Corona",
                "country" : "Stati Uniti d'America",
                "countrycode" : "US",
                "county" : "Lincoln County",
                "countycode" : null,
                "hash" : null,
                "house" : null,
                "latitude" : "34.150169",
                "line1" : null,
                "line2" : "Lon, NM 88318",
                "line3" : null,
                "line4" : "Stati Uniti d'America",
                "longitude" : "-105.122032",
                "name" : null,
                "neighborhood" : "Lon",
                "offsetlat" : "34.150169",
                "offsetlon" : "-105.122032",
                "postal" : "88318",
                "quality" : "50",
                "radius" : "700",
                "state" : "Nuovo Messico",
                "statecode" : "NM",
                "street" : null,
                "unit" : null,
                "unittype" : null,
                "uzip" : "88318",
                "woeid" : "2441272",
                "woetype" : "7",
                "xstreet" : null
              },
              { "city" : "Rogersville",
                "country" : "Stati Uniti d'America",
                "countrycode" : "US",
                "county" : "Webster County",
                "countycode" : null,
                "hash" : null,
                "house" : null,
                "latitude" : "37.183601",
                "line1" : null,
                "line2" : "Lon, MO 65742",
                "line3" : null,
                "line4" : "Stati Uniti d'America",
                "longitude" : "-93.059196",
                "name" : null,
                "neighborhood" : "Lon",
                "offsetlat" : "37.183601",
                "offsetlon" : "-93.059196",
                "postal" : "65742",
                "quality" : "50",
                "radius" : "700",
                "state" : "Missouri",
                "statecode" : "MO",
                "street" : null,
                "unit" : null,
                "unittype" : null,
                "uzip" : "65742",
                "woeid" : "2441273",
                "woetype" : "7",
                "xstreet" : null
              }
            ] }
    } }
