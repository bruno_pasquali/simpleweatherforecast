describe("ajaxList-WeatherOnLine-JSON", function() {

	myWF.isLocal = true;
    var ajaxList = null;
    var formatTime = function (date) {
        var h = date.getUTCHours();
        var ampm = (h >= 12)? (" PM"): " AM";
        if (h > 12) { h -= 12; }
        m = date.getUTCMinutes();
        return ((h>9)?h.toString():'0'+h) + ':' + ((m>9)?m.toString():'0'+m) + ampm;
    };
    var formatDate = function (date) {
        var h = date.getUTCHours();
        return (date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate());
    };

    beforeEach(function() {
		ajaxList = myWF.createGeoAjaxList(
                myWF.createForecastList_WorldWeatherOnLine()
        );
    });

    it("Gets an URL by its service", function() {
        expect(ajaxList.serviceAdapter.getURL('Adro,Italy')).not.toEqual('');
    });
    
    it("Puts an error for empty data by its service", function() {
        var error = {};
        var list = ajaxList.serviceAdapter.convertHead(errorTestData, null, error);
        expect(error.code).not.toEqual('');
        expect(error.detail).toEqual("Unable to find any matching weather location to the query submitted!");
        expect(list).toEqual([]);
    });
    
    it("Converts to a forecast by its service", function() {
        var error = {};
        var head = {};
        var service = ajaxList.serviceAdapter;
        var rawList = service.convertHead(testData, head, error);
        expect(!error['code']).toEqual(true);
        expect(rawList.length).toEqual(5);
        var list = myWF.util.map(rawList, function(e) {
            return service.convertElement(e);
        });
        expect(list.length).toEqual(5);

        var current = head.currentConditions; 
        expect(current.constructor).toBe(myWF.Forecast);
        expect(current.wind.speed).toEqual(17);
        expect(current.maxTemperature).toEqual(16);
        expect(current.minTemperature).toEqual(7);
        expect(current.humidity).toEqual(62);
        expect(current.visibility).toEqual(10);
        expect(current.temperature).toEqual(9);
        expect(current.cloudCover).toEqual(75);
        expect(current.pressure).toEqual(1030);
        expect(current.precipitationMM).toEqual(1.2);
        expect(current.iconUrl).toEqual(myWF.util.forecastImagesLocalBaseUrl + '44.png');
        expect(formatTime(current.observationTime)).toEqual(testData.data.current_condition[0].observation_time); 
        expect(formatDate(current.date)).toEqual(testData.data.weather[0].date); 

        expect(list[1].date).toEqual(new Date('2011-10-16'));
        expect(list[1].wind.getSpeed()).toEqual(7);
        expect(list[1].wind.getSpeed(myWF.Wind.MILES_PER_HOUR)).toEqual(Math.round(7*0.621371192));
        expect(list[1].wind.getSpeed(myWF.Wind.KNOTS)).toEqual(Math.round(7 * 1.852));
        expect(list[1].wind.direction).toEqual('SSE');
        expect(list[1].precipitationMM).toEqual(0.1);

        expect(list[1].customCode).toEqual('113');
        expect(list[2].customDescription).toEqual('Sunny');
        expect(list[3].maxTemperature).toEqual(16);
        expect(list[3].minTemperature).toEqual(4);
        expect(list[4].wind.direction).toEqual('S');
        expect(list[4].iconUrl).toEqual(myWF.util.forecastImagesLocalBaseUrl + '28.png');
    });

    it("Reports failed calls", function() {
        var status = ajaxList.status;
        ajaxList.serviceAdapter.getURL = function () { return 'http://xxx.yyy.aaa'; };
        spyOn(ajaxList, 'onFail');
        ajaxList.update("Venice,US");
        waitsFor(function() {
            return (status.isNotOk());
        }, "Never fired OnFail",    0);
        runs(function() {
            expect(ajaxList.onFail).toHaveBeenCalled();
        });
    });    
    
    it("Gets a list of forecasts by geolocation", function() {
        var status = ajaxList.status;
        ajaxList.onSuccess = function () {};
        spyOn(ajaxList, 'onSuccess');
        ajaxList.update("1.0, 1.0");
        waitsFor(function() {
            return (status.isOk());
        }, "Never got list of forecasts by geolocation", 1000);
        runs(function() {
            var list = ajaxList.data.list;
            var current = ajaxList.data.currentConditions;
            expect(ajaxList.onSuccess).toHaveBeenCalled();
            expect(list.length).not.toEqual(0);
            expect(list[0].date).not.toEqual("");
            expect(list[0].constructor).toBe(myWF.Forecast);
            expect(list[1].maxTemperature).toBeGreaterThan(-100); //trap NaN
            expect(formatTime(current.observationTime)).toEqual(ajaxList.rawData.data.current_condition[0].observation_time); 
        });
    });

    it("Gets a list of forecasts by city name", function() {
        var success = false;
        ajaxList.onSuccess = function () { 
            success = true; 
        };
        spyOn(ajaxList, 'onSuccess').andCallThrough();
        ajaxList.update('Adro, Italy');
        waitsFor(function() {
            return success;
        }, "Never got a list of forecasts by city name", 1000);
        runs(function() {
            expect(ajaxList.onSuccess).toHaveBeenCalled();
            expect(ajaxList.data.list.length).not.toEqual(0);
            expect(ajaxList.data.list[0].date).not.toEqual("");
            expect(ajaxList.data.currentConditions.date).not.toEqual("");
            expect(ajaxList.data.list[0].constructor).toBe(myWF.Forecast);
            expect(ajaxList.data.list[1].maxTemperature).toBeGreaterThan(-100); //trap NaN
        });
    });

    var errorTestData = { 
        "data": 
            { "error": [{
                "msg": "Unable to find any matching weather location to the query submitted!" 
            }]}};
    
    var testData = {
      "data":{
        "current_condition":[
          {
            "cloudcover":"75",
            "humidity":"62",
            "observation_time":"09:34 AM",
            "precipMM":"1.2",
            "pressure":"1030",
            "temp_C":"9",
            "temp_F":"48",
            "visibility":"10",
            "weatherCode":"116",
            "weatherDesc":[
              {
                "value":"Partly Cloudy"
              }
            ],
            "weatherIconUrl":[
              {
                "value":"http:\/\/www.worldweatheronline.com\/images\/wsymbols01_png_64\/wsymbol_0002_sunny_intervals.png"
              }
            ],
            "winddir16Point":"ESE",
            "winddirDegree":"120",
            "windspeedKmph":"17",
            "windspeedMiles":"11"
          }
        ],
        "request":[
          {
            "query":"Adro, Italy",
            "type":"City"
          }
        ],
        "weather":[
          {
            "date":"2011-10-15",
            "precipMM":"0.6",
            "tempMaxC":"16",
            "tempMaxF":"60",
            "tempMinC":"7",
            "tempMinF":"44",
            "weatherCode":"113",
            "weatherDesc":[
              {
                "value":"Sunny"
              }
            ],
            "weatherIconUrl":[
              {
                "value":"http:\/\/www.worldweatheronline.com\/images\/wsymbols01_png_64\/wsymbol_0001_sunny.png"
              }
            ],
            "winddir16Point":"SSW",
            "winddirDegree":"207",
            "winddirection":"SSW",
            "windspeedKmph":"8",
            "windspeedMiles":"5"
          },
          {
            "date":"2011-10-16",
            "precipMM":"0.1",
            "tempMaxC":"16",
            "tempMaxF":"62",
            "tempMinC":"5",
            "tempMinF":"41",
            "weatherCode":"113",
            "weatherDesc":[
              {
                "value":"Sunny"
              }
            ],
            "weatherIconUrl":[
              {
                "value":"http:\/\/www.worldweatheronline.com\/images\/wsymbols01_png_64\/wsymbol_0001_sunny.png"
              }
            ],
            "winddir16Point":"SSE",
            "winddirDegree":"154",
            "winddirection":"SSE",
            "windspeedKmph":"7",
            "windspeedMiles":"4"
          },
          {
            "date":"2011-10-17",
            "precipMM":"0.0",
            "tempMaxC":"16",
            "tempMaxF":"60",
            "tempMinC":"3",
            "tempMinF":"38",
            "weatherCode":"113",
            "weatherDesc":[
              {
                "value":"Sunny"
              }
            ],
            "weatherIconUrl":[
              {
                "value":"http:\/\/www.worldweatheronline.com\/images\/wsymbols01_png_64\/wsymbol_0001_sunny.png"
              }
            ],
            "winddir16Point":"S",
            "winddirDegree":"187",
            "winddirection":"S",
            "windspeedKmph":"5",
            "windspeedMiles":"3"
          },
          {
            "date":"2011-10-18",
            "precipMM":"0.0",
            "tempMaxC":"16",
            "tempMaxF":"62",
            "tempMinC":"4",
            "tempMinF":"40",
            "weatherCode":"113",
            "weatherDesc":[
              {
                "value":"Sunny"
              }
            ],
            "weatherIconUrl":[
              {
                "value":"http:\/\/www.worldweatheronline.com\/images\/wsymbols01_png_64\/wsymbol_0001_sunny.png"
              }
            ],
            "winddir16Point":"SSW",
            "winddirDegree":"200",
            "winddirection":"SSW",
            "windspeedKmph":"6",
            "windspeedMiles":"4"
          },
          {
            "date":"2011-10-19",
            "precipMM":"0.8",
            "tempMaxC":"15",
            "tempMaxF":"59",
            "tempMinC":"6",
            "tempMinF":"43",
            "weatherCode":"119",
            "weatherDesc":[
              {
                "value":"Cloudy"
              }
            ],
            "weatherIconUrl":[
              {
                "value":"http:\/\/www.worldweatheronline.com\/images\/wsymbols01_png_64\/wsymbol_0003_white_cloud.png"
              }
            ],
            "winddir16Point":"S",
            "winddirDegree":"173",
            "winddirection":"S",
            "windspeedKmph":"7",
            "windspeedMiles":"4"
          }
        ]
      }
    };
 
});
