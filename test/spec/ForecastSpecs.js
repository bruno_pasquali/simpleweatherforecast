describe("Forecast", function() {
  var forecast = {};

  beforeEach(function() {
    forecast = new myWF.Forecast();
  });

  it("has some properties", function() {
	    expect(forecast.temperature).toEqual(0);
	    expect(forecast.maxTemperature).toEqual(0);
	    expect(forecast.minTemperature).toEqual(0);
	    expect(forecast.weatherDescription).toEqual("");
  });

  //demonstrates use of expected exceptions
  describe("#resume", function() {
	  it("should throw an exception if Constructor called without new", function() {
      expect(function() {
          myWF.Forecast();
      }).toThrow("Contructor called without new");
    });
  });
});