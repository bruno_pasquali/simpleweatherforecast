describe("Storage", function() {

  beforeEach(function() {
      var self = this;
  });
  
  it("It can store and retrieve objects data", function() {
        var o = {
                id: "123456",
                inner: {inside: "get me !"},
                list: [1, 2, "w"],
                string: "any description",
                value: 1234567890,
                doNotSaveMe: function() { return "Going to be lost"; }
        };  
        //:todo setter for temp ids for testing environment
        storage = myWF.createStorage();
        var json = storage.save(o.id,o);
        expect(json.substr(0,9)).toEqual('{"id":"12');
        otherStorage = myWF.createStorage();
        oo = otherStorage.load(o.id);
        expect(oo.id).toEqual("123456");
        expect(oo.inner.inside).toEqual("get me !");
        expect(oo.list).toEqual([1, 2, "w"]);
        expect(oo.string).toEqual("any description");
        expect(oo.value).toEqual(1234567890);
        expect(typeof oo["doNotSaveMe"]).toEqual("undefined");
        storage.clear(oo.id);
  });
  //:todo mancano test
  it("I can use it to save and restore objects with methods", function() {
      self.ObjectCreator = function () {
          this.id = 12345;
          this.someInfo = "some info";
          this._creator = "ObjectCreator";
          this.someMethod = function () { return "some method"; };
      };
      var o = new self.ObjectCreator ();
      storage = myWF.createStorage();
      var json = storage.save(o.id,o);
      //storage.saveSettingsData(o);
      otherStorage = myWF.createStorage();
      jo = otherStorage.load(o.id);
      oo = new self[jo._creator]();
      for (p in jo) {
          if (p !== "_creator") {
              oo[p] = jo [p];
          }
      }
      expect(jo.id).toEqual(12345);
      expect(oo.id).toEqual(12345);
      expect(oo.someMethod()).toEqual("some method");
      storage.clear(o.id);
});

});