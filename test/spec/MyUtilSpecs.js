describe("My spare utilities", function() {

	it("Maps", function() {
		var array = [1, 2, 3];
		var rarray = myWF.util.map(array, function (item, index, parray) {
			return item*2;
		});
		expect(rarray[1]).toEqual(4);
		/*
        var result = array.myReduce(function (accumulator, item, index, parray) {
            return item+accumulator;
        }, 0);
        expect(result).toEqual(6);
        */
	});

	it("Iterates", function() {
		var array = [1, 2, 3];
		myWF.util.iterate(array,function (item, index, parray) {
			expect(parray[index]).toEqual(item);
			expect(parray[index]).toEqual(array[index]);
		});
	});

    it("Has a locale", function() {
        var locale = myWF.util.getLocale();
        myWF.util.setLocale('');
        expect(myWF.util.getLocale()).not.toEqual('');
        myWF.util.setLocale(locale);
    });
    it("Does ajax GET asyncronous requests", function() {

		var theEnd = false;
		var gotData = {};
		var onSuccess = function (data, status) {
			gotData = data;
			theEnd = true;
		};
		var onFail = function (status, exception) {
		};
		
        var url = 'http://www.google.com';
		myWF.util.ajax(url, onSuccess, onFail);
		waitsFor(function() {
			return theEnd;
		}, "Never completed the rigth ajax request", 1000);
		runs(function() {
			expect(gotData).not.toEqual({});
		});
	});
	/*
    it("Fails on wrong ajax asyncronous requests", function() {
		var withError = false;
		var onSuccess = function (data, status) {
		};
		var onFail = function (status, exception) {
			withError = true;
		};
		
		myWF.util.ajax("http://xxx.yyy.aaa", onSuccess, onFail);
		waitsFor(function() {
			return withError;
		}, "Never completed the wrong ajax request", 10000);
		runs(function() {
			expect(withError).toEqual(true);
		});
	});
	*/	
});