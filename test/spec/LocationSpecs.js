describe("Location", function() {
	var location = null,
		myLocations = myWF.createItemsBag({
		    load: myWF.storage.loadLocationData,
		    save: myWF.storage.saveLocationData,
		    ItemConstructor: myWF.Location
		});        

	beforeEach(function() {
		location = new myWF.Location();
	});

	it("Has default properties", function() {
		var o = {
			city: 1,
			state: 1,
			country: 1,
			countryCode: 1
		};
		for (var p in o) {
			if (o.hasOwnProperty(p)) {
				expect(location[p]).toEqual("");
			}
		}
		expect(location.latitude).toEqual(null);
		expect(location.longitude).toEqual(null);
	});

	describe("My Location", function() {
		var l1, l2, l3;
		var s1 = "abc", s2 = "cba", s3 = "zzz";
		
		beforeEach(function() {
			myLocations.empty();
			l1 = new myWF.Location();
			l1.city = s2;
			l1.state = s1;
			l1.country = s3;
			l2 = new myWF.Location();
			l2.city = s3;
			l2.state = s1;
			l2.country = s2;
			l3 = new myWF.Location();
			l3.city = s1;
			l3.state = s2;
			l3.country = s3;
			myLocations.addUnsorted(l1);
			myLocations.addUnsorted(l2);
			myLocations.addUnsorted(l3);
		});

		it("Can be emptied", function() {
			myLocations.empty();
			expect(myLocations.length()).toEqual(0);
		});
		it("Adds and gets unsorted locations", function() {
			expect(myLocations.getByIndex(0)).toBe(l1);
			expect(myLocations.getByIndex(1)).toBe(l2);
		});
		it("Removes locations and has a length", function() {
			myLocations.remove(l1);
			myLocations.remove(l2);
			expect(myLocations.length()).toEqual(1);
			expect(myLocations.getByIndex(0)).toBe(l3);
		});
		it("Adds unique locations", function() {
			myLocations.addUnsorted(l1);
			myLocations.addUnsorted(l2);
			myLocations.addUnsorted(l2);
			expect(myLocations.length()).toEqual(3);
			expect(myLocations.getByIndex(0)).toBe(l1);
			expect(myLocations.getByIndex(2)).toBe(l3);
		});
		it("Sorts locations added by 'add()'", function() {
			myLocations.add(l3);
			myLocations.add(l3);
			expect(myLocations.getByIndex(0)).toBe(l3);
			expect(myLocations.getByIndex(1)).toBe(l1);
			expect(myLocations.getByIndex(2)).toBe(l2);
			myLocations.remove(l1);
			expect(myLocations.getByIndex(0)).toBe(l3);
			expect(myLocations.getByIndex(1)).toBe(l2);
		});
		it("Can be saved and restored", function() {
			var ver = function(i, l) {
					var g = myLocations.getByIndex(i);
					expect(myWF.util.toJSON(g)).toEqual(myWF.util.toJSON(l));
					expect(g.constructor).toEqual(l.constructor);
				};
			myLocations.save();
			myLocations.empty();
			myLocations.load();
			ver(0, l1);
			ver(1, l2);
			ver(2, l3);
		});
		
		myLocations.empty();
		
	});

});