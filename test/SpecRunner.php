<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
  "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Jasmine Spec Runner</title>

  <link rel="shortcut icon" type="image/png" href="lib/jasmine-1.2.0/jasmine_favicon.png">
  <link rel="stylesheet" type="text/css" href="lib/jasmine-1.2.0/jasmine.css">
  <script type="text/javascript" src="lib/jasmine-1.2.0/jasmine.js"></script>
  <script type="text/javascript" src="lib/jasmine-1.2.0/jasmine-html.js"></script>

  <!-- include source files here... 
<script type="text/javascript" src="../www-src/lib/jquery-1.6.4.js"></script>
<script type="text/javascript" charset="utf-8"
	src="../www-src/lib/jquery.json-2.3.min.js"></script>
-->

<script type="text/javascript" src="../www-src/src/util.js"></script>
<script type="text/javascript" src="../www-src/src/Keys.js"></script>
<script type="text/javascript" src="../www-src/src/Storage.js"></script>

<script type="text/javascript" src="../www-src/src/Wind.js"></script>
<script type="text/javascript" src="../www-src/src/Settings.js"></script>
<script type="text/javascript" src="../www-src/src/Location.js"></script>
<script type="text/javascript" src="../www-src/src/MyLocations.js"></script>
<script type="text/javascript" src="../www-src/src/Forecast.js"></script>
<script type="text/javascript" src="../www-src/src/GeoAjaxList.js"></script>
<script type="text/javascript"
	src="../www-src/src/ForecastList_WWOL.js"></script>
<script type="text/javascript"
	src="../www-src/src/LocationList_YAHOO.js"></script>

  <!-- include spec files here... -->
<script type="text/javascript" src="spec/MyUtilSpecs.js"></script>

<script type="text/javascript" src="spec/SettingsSpecs.js"></script>
<script type="text/javascript" src="spec/ForecastSpecs.js"></script>
<script type="text/javascript" src="spec/StorageSpecs.js"></script>
<script type="text/javascript" src="spec/ForecastList_WWOL_Specs.js"></script>
<script type="text/javascript" src="spec/LocationSpecs.js"></script>
<script type="text/javascript" src="spec/LocationList_YAHOO_Specs.js"></script>

  <script type="text/javascript">
    (function() {
      var jasmineEnv = jasmine.getEnv();
      jasmineEnv.updateInterval = 1000;

      var htmlReporter = new jasmine.HtmlReporter();

      jasmineEnv.addReporter(htmlReporter);

      jasmineEnv.specFilter = function(spec) {
        return htmlReporter.specFilter(spec);
      };

      var currentWindowOnload = window.onload;

      window.onload = function() {
        if (currentWindowOnload) {
          currentWindowOnload();
        }
        execJasmine();
      };

      function execJasmine() {
        jasmineEnv.execute();
      }

    })();
  </script>

</head>

<body>
</body>
</html>
