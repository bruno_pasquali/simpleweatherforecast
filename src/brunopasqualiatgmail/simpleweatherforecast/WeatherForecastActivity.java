package brunopasqualiatgmail.simpleweatherforecast;

import android.app.Activity;
import android.os.Bundle;
import brunopasqualiatgmail.simpleweatherforecast.R;
import org.apache.cordova.*;

public class WeatherForecastActivity extends DroidGap {
	//hack for Android 2.* Devices
	//https://groups.google.com/forum/?fromgroups=#!topic/phonegap/x5J3tfKpht0
	private String jsUrl = "onAndroidPhoneBackKeyDown()";
	@Override
    public void onBackPressed() {  
 		super.loadUrl("javascript:"+jsUrl);
	    return;
    }
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        super.setBooleanProperty("keepRunning", false);
        super.setIntegerProperty("loadUrlTimeoutValue", 30000); //added
        super.setIntegerProperty("splashscreen", R.drawable.splash);
 		super.loadUrl("file:///android_asset/www/touchs.html", 20000);
    }
}
