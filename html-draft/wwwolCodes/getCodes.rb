#!/usr/bin/env ruby 
require 'net/http'
require 'rexml/document'
require 'erb'
require 'yaml'

url = 'http://www.worldweatheronline.com/feed/wwoConditionCodes.xml'
# get the XML data as a string
xml_data = ''
xml_data = Net::HTTP.get_response(URI.parse(url)).body
File.open('codes.xml', 'w') { |f| f.write xml_data }  
#File.open('codes.xml', 'r') { |f| xml_data = f.read }  

class Code
  attr_accessor :code, :description, :day_icon, :night_icon
  def to_csv
    "#{code},\"#{description}\",#{day_icon},#{night_icon}"
  end
  alias to_s :to_csv
end

NewImagesNames = []
Dir.glob('sym/*.png').each {|f| NewImagesNames << Hash[:fileName => f, :id => File.basename(f, ".png")]}
NewImagesNames.sort! do |a, b|
  # fa = a[:id].sub('_', '.').to_f  
  # fb = b[:id].sub('_', '.').to_f  
  # fa <=> fb
  a[:id].to_i <=> a[:id].to_i
end


def parseCodes(xml_data)
  codes = []
  doc = REXML::Document.new(xml_data)
  doc.elements.each('codes/condition') do |ele|
    code = Code.new 
    code.code = ele.elements['code'].text.strip
    code.description = ele.elements['description'].text.strip
    code.day_icon = ele.elements['day_icon'].text.strip
    code.night_icon = ele.elements['night_icon'].text.strip
    codes << code
  end
  codes.sort! { |a, b|  a.code.to_i <=> b.code.to_i }
end

def getIcons(codes) 
  Net::HTTP.start("www.worldweatheronline.com") do |http|
    codes.each do |code|
      resp = http.get("/images/wsymbols01_png_64/"+code.day_icon+".png")
      File.open("images/"+code.day_icon+".png", "wb") { |file|
        file.write(resp.body)
      }
      resp = http.get("/images/wsymbols01_png_64/"+code.night_icon+".png")
      File.open("images/"+code.night_icon+".png", "wb") { |file|
        file.write(resp.body)
      }
    end
  end
end

def generateHTML(codes)
  erb = ERB.new File.new("codes.html.erb").read, nil, "%"
  File.open('codes.html', 'w') { |f| f.write erb.result(TOPLEVEL_BINDING) }  
end

def dumpYAML(codes)
  File.open('codes.yaml', 'w') { |f| f.puts YAML::dump(codes) }  
end

def dumpCSV(codes)
  File.open('codes.csv', 'w') { |csv|
    codes.each { |code| csv.puts code.to_csv }  
  }
end

Codes = parseCodes xml_data
dumpYAML Codes
generateHTML Codes
#getIcons Codes
dumpCSV Codes
#File.open('codes.yaml', 'r') { |f| csv.puts YAML::load(f.read) }  
    
__END__

<codes>
  <condition>
    <code>395</code>
    <description>Moderate or heavy snow in area with thunder</description>
    <day_icon>wsymbol_0012_heavy_snow_showers</day_icon>
    <night_icon>
      wsymbol_0028_heavy_snow_showers_night</night_icon>
  </condition>
  ..
</codes>
