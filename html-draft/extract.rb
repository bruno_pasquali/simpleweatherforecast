File.open("output.html", "w") { |fout|  
File.open("f.html", "r") { |fin|  
  loop do
    while (line = fin.gets) != nil do
      break if line.strip.include? '"text/x-handlebars-template"'
    end
    break if line == nil 
    fout.puts '===========  ' + line 
    while (line = fin.gets) != nil do
      count = 0
      count += 1 while line[count] == ' ' 
      line.strip!
      break if line == '</script>'
      line = line.sub('{{{', '{').sub('{{', '{').sub('}}}', '}').sub('}}', '}')
      fout.puts ' ' * count + %(') + line + %(') + ','
    end
    break if line == nil 
  end
}}