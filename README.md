## Simple weather forecast app for Android

A simple application to test and learn javascript development for Android with Phonegap & Sencha Touch. 
The souces are located in "www-src" folder, while the minified files, ready for phonegap build, are located in "assets". 
The stable version has tag "1.0".
The APK binary is available on the [Google Play Market](https://play.google.com/store/apps/details?id=brunopasqualiatgmail.simpleweatherforecast). 

Before building the apk you must run "rake deploy" (need ruby & rake). 
To test the "touchs-debug.html" page on a desktop browser, you will need a server with reverse proxy configuration: see in "apache-conf/extra"

You will also need to:

- install sencha touch SDK: [http://www.sencha.com/products/sdk-tools/download/](http://http://www.sencha.com/products/sdk-tools/download/). 
- Install the android SDK: [http://developer.android.com/sdk/index.html](http://developer.android.com/sdk/index.html).
- Install the ADT plug-in for eclipse: [http://developer.android.com/tools/sdk/eclipse-adt.html](http://developer.android.com/tools/sdk/eclipse-adt.html).
- Obtain your API key from [http://www.worldweatheronline.com](http://www.worldweatheronline.com),
- an Application ID from [YAHOO](http://developer.apps.yahoo.com)
- and then to declare the two keys in some source file:

    myWF.keys.WWOL = (your key);

    myWF.keys.YAHOO_appid = (your key); 