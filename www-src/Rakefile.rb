require 'rake/clean'
require './html.rb'

JS_COMPRESSOR = "closurecompiler" #uglifyjs, yuicompressor, closurecompiler
TARGET_PATH = File.expand_path('../assets/www')
MINI_JS = "script.js"
APP_JS = "ui-s/app.js"
APP_BUILD_JS = "ui-s/build/all-classes.js"
# chdir('www-src')

CLEAN.add('all-classes.js')
CLEAN.add('app-all.js')
#CLEAN.add('app.jsb3')
CLEAN.add('ui-s/build/tmp.html')
CLEAN.add(Dir.glob('../bin/*.apk'))
#CLEAN.add(Dir.glob("#{APP_BUILD_JS}-temp*"))
CLEAN.add(Dir.glob('ui-s/archive'))
CLEAN.add(Dir.glob('app-all.js-temp*'))
CLOBBER.add(TARGET_PATH)
CLOBBER.add(APP_BUILD_JS)
CLOBBER.add('touch*.html')
CLOBBER.add('src.js')
CLOBBER.add('touchs-*.html')
CLOBBER.add('ui-s/resources/css/android.css')

APP_CSS = ["ui-s/resources/css/app.css"]
THEMES = ["android", "apple", "metro", "bb10"]
def themeCSSPath(theme)
  "ui-s/resources/css/#{theme}.css"
end
def themeMiniCSSPath(theme)
  "ui-s/resources/css/#{theme}_mini.css"
end
def themeSCSSPath(theme)
  "ui-s/resources/sass/#{theme}.scss"
end
THEMES_CSS = FileList.new
MINI_CSS = FileList.new
THEMES.each do |theme| 
  THEMES_CSS.add(themeCSSPath(theme))
  MINI_CSS.add(themeMiniCSSPath(theme))
end
#MINI_CSS.gsub!("_debug", "")
#MINI_CSS.gsub!("/scc/", "/sass/")

JS_DEPLOY_SOURCES = FileList.new(
    "lib/cordova-2.3.0.js",
#        "ui-s/sdk/sencha-touch.js",
        #cmd3
        "ui-s/touch/sencha-touch.js",
    "ui-s/senchaUtil.js",
    "src/util.js",
    "src/Keys.js",
    "src/Storage.js",        
    "src/Wind.js",
    "src/Settings.js",
    "src/Location.js",
    "src/MyLocations.js",
    "src/Forecast.js",
    "src/ForecastList_WWOL.js",
    "src/LocationList_YAHOO.js",
    "src/GeoAjaxList.js",
    "src/boot.js",
        APP_BUILD_JS
)
#used for debug

JS_SOURCES = FileList.new(JS_DEPLOY_SOURCES.to_a)
JS_SOURCES.exclude(APP_BUILD_JS)
JS_SOURCES.gsub!("sencha-touch.js", "sencha-touch-debug.js")

#no more needed by cmd3
JS_DEPLOY_SOURCES.exclude("ui-s/touch/sencha-touch.js")

#I do not know how to exclude app.js from "sencha compile", so I added it in html
#JS_SOURCES.gsub!(APP_BUILD_JS, APP_JS)

JS_UI_SOURCES = FileList.new [
	APP_JS,
	"ui-s/view/*.js"
]	

APK_SOURCES = FileList.new [
	"touchs.html",
	"app.png",
	MINI_JS,
	"splash.css",
	"images/*",
	"ui-s/resources/images/*",
] + MINI_CSS

APK_TARGETS = FileList.new 
APK_SOURCES.each do |source|
	target = File.expand_path(File.join(TARGET_PATH, source))
	APK_TARGETS.add target
	directory File.dirname(target)
	file target => [File.dirname(target), source] do
		cp_r source, File.dirname(target)
	end
end

task :default => :testBuild 

directory "ui-s/build"

desc "Prepare deploy folder to build the app's apk archive"
task :deploy => ["ui-s/build"] + APK_TARGETS 

desc "Prepare deploy folder for debug an tests on emulator: copy of sencha sdk full sources"
task :deploy_debug => ["ui-s/resources/css/android.css", "touchs-debug.html"] do
  cp "touchs-debug.html", File.join(TARGET_PATH, "touchs.html")
  sources = FileList.new(JS_UI_SOURCES)
  sources.add(
    "app.png",
    "*.css",
    "images/*",
    "ui-s/senchaUtil.js",
    "loader.js",
    "ui-s/resources/images/*",
    "ui-s/resources/css/app.css",
    "ui-s/resources/css/android.css",

    #cmd3
    "ui-s/touch/sencha-touch-debug.js", 
    "ui-s/touch/src/*", 

    "lib/cordova-2.3.0.js") 
  sources.add(JS_SOURCES) 
  
  #sources.add("ui-s/sdk/microloader/*") 
  #sources.add("ui-s/resources/css/android.css") 
  sources.each do |source|
    target = File.expand_path(File.join(TARGET_PATH, source))
    mkdir_p File.dirname(target) 
    cp_r source, target
  end
end

desc "Builds only uncompressed files for testing in browser"
task :testBuild => ["touchs.html"] + THEMES_CSS 

desc "compile sencha scss file to css"
task :css do 
	cd = Dir.pwd
	Dir.chdir('ui-s/resources')
    sh "compass compile sass"
	Dir.chdir(cd)
end

desc "compile app erb file to 2 html: for deploy and tests"
task :html do 
    hg = HtmlGenerator.new("touchs.html.erb")
    THEMES.each do |theme|
      hg.run(JS_SOURCES, [themeCSSPath(theme)] + APP_CSS, "true", "true", "touchs-debug-#{theme}.html")
      hg.run([MINI_JS], [themeMiniCSSPath(theme)], "false", "false", "touchs-#{theme}.html")
    end
    cp "touchs-android.html", "touchs.html"
end

def minify(source, target) 
  sh "sencha fs minify -f #{source} -t #{target} -c #{JS_COMPRESSOR}"  
  #cp source, target
end
def compile(name, prerequisites) 
  temp = "#{name}.temp.js"
  sh "sencha fs concat -f #{prerequisites.join(',')} -t #{temp}"
  minify(temp, name)  
  rm temp
end

#desc "extract needed sencha js files"
#file "all-classes.js" => JS_UI_SOURCES do
  #sh "sencha create jsb -a touchs-debug.html -p app.jsb3"
  #sh "sencha build -p app.jsb3 -d ."
  #cmd3
#end

#cmd3
#file "ui-s/app-build.js" => ["all-classes.js", "ui-s/app.js"] do |t|
file APP_BUILD_JS => JS_UI_SOURCES do |t|
  sh 'sencha compile -classpath=ui-s/touch/src,ui-s/view page -in touchs-debug-android.html -out ui-s/build/tmp.html'
  #compile(t.name, t.prerequisites)
end

file MINI_JS => JS_DEPLOY_SOURCES do |t|
  compile(t.name, t.prerequisites)
end
THEMES.each do |theme|
  file themeCSSPath(theme) => themeSCSSPath(theme) do
    Rake::Task['css'].invoke  
  end
  file themeMiniCSSPath(theme) => [themeCSSPath(theme)] + APP_CSS do |t|
    sh "sencha fs concat -f #{t.prerequisites.join(',')} -t #{t.name}"
  end
end
file "touchs.html" => ["touchs.html.erb", "html.rb"] do
	Rake::Task['html'].invoke	
end
