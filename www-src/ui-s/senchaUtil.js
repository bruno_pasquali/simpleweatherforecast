(function(ns) {
    
    var handler = function (box, action, event, node, options, eOpts) {
        var x = event.pageX;
        var y = event.pageY;

        if ((x < box.x) || (y < box.y) || (x > box.right) || (y > box.bottom)) {
            return;
        }    
        ns.util.vibrate();
        if (typeof(action) === 'string') {
            this[action](event, node, options, eOpts);   
        }
        else {
            action.call(this, event, node, options, eOpts);   
        }
    };
    ns.sencha = {};
    //on my Android phone and emulators some times tap event is missed, while touchend is not
    ns.sencha.buttonListenAt = function (elementQuery, action, contest){
        var that = contest || this,
            element = that.query(elementQuery)[0].element;
            
        element.on({
            scope: that,
            touchend: function (event, node, options, eOpts) {
                handler.call(this, element.getBox(), action, event, node, options, eOpts);
            }
//            ,touchstart: function (event, node, options, eOpts) {
//                ns.util.vibrate();
//            }
        });
    };

})(myWF);
