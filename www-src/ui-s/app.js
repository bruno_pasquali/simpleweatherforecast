var onAndroidPhoneBackKeyDown = function () {
    console.log('calling empty onAndroidPhoneBackKeyDown()');
};

(function () {
    var _alert = myWF.messages.alert,
        _confirm= myWF.messages.confirm,
        _buttonListenAt = myWF.sencha.buttonListenAt;
        
    var init = function () {

		var panel = Ext.create('Ext.Panel', {
		    forecastView: null,
			layout: 'card',
			//layout: 'fit',
			items: [
	            /*
	            Ext.create('MyWF.view.Splash')  
	            ,forecastPanel,
	            settingsPanel,
	            myLocationsPanel,
	            addLocationPanel,
	            infoPanel,
	            forecastDetailPanel
	            */
			]
		}),
		refreshButton = Ext.create('Ext.Button', {	
            iconMask: true, ui: 'normal', iconCls: 'refresh', 
            itemId: 'refreshButton'
            //,listeners: { tap: function () { forecastPanel.updateForecast(); } }
		}),
		mainToolbar = Ext.create('Ext.Toolbar', {				
			docked: 'bottom',
			layout: { pack: 'center' },
			//height: '120%',
			items: [
				{ 
					iconMask: true, ui: 'normal', iconCls: 'settings',  
                    itemId: 'settingsButton'
					//listeners: { tap: function () { pages.setPage('p_settings'); } }
				},
                { xtype: 'spacer' },
				refreshButton,
		        { xtype: 'spacer' },
				{ 
					iconMask: true, ui: 'normal', iconCls: 'favorites', 
                    itemId: 'favoritesButton'
					// listeners: {tap: function () { 
						// pages.setPage('p_locations'); 
					// }}
				},
				{ 
					iconMask: true, ui: 'normal', iconCls: 'add', 
					//pressedCls: 'customPressedStyle',
                    itemId: 'addLocationButton'
					// listeners: {tap: function () { 
						// pages.setPage('p_addLocation'); 
					// }}
				}
			]	
		}),
		backToolbar = Ext.create('Ext.Toolbar', {				
			xtype: 'toolbar',
			docked: 'bottom',
			layout: { pack: 'center' },
			items: [
                { xtype: 'spacer' }
                ,{ 
                    iconMask: true, ui: 'normal', iconCls: 'reply',
                    itemId: 'backToHomeButton'
                    // listeners: { tap: function () {
                            // goToSplashPage(); 
                        // }
                    // }
                }
			]
		});
		//var forecastPanel = null;

        // refreshButton.on('tap', function () { 
            // panel.forecastView.updateForecast(); 
        // });

		var showView = function (viewName, beforeShow) {
			panel.removeAll(true, true);
			var view = Ext.create(viewName);
			if (beforeShow) { beforeShow(view) };
			panel.setItems(view);
			panel.setActiveItem(view);
			return view;
		};
        var pages = {
            backPage: '',
            currentPage: '',
			setPage: function (page, arg1, arg2) {
				if (page in pages) { 
				    pages[page](arg1, arg2); 
                    pages.currentPage = page;
				 }
			},
			showMainToolbar: function () {
				mainToolbar.show();
				backToolbar.hide();
			},
            showBackToolbar: function () {
                backToolbar.show();
                mainToolbar.hide();
            },
            hideToolbars: function () {
                backToolbar.hide();
                mainToolbar.hide();
            },
			'p_splash': function () {
                pages.showMainToolbar();
                pages.backPage = '';
                if (!myWF.myLocations.currentItem 
                    && (myWF.myLocations.length() === 1)) {
                    myWF.myLocations.currentItem = myWF.myLocations.getByIndex(0);                    
                }
                if (myWF.myLocations.currentItem) {
					//panel.setActiveItem(1);
					showView('MyWF.view.Forecast', function(view) {
				        view.onDaySelected = function (location, forecast) { 
				            pages.setPage('p_daydetail', location, forecast);
				        };
				        view.onTodaySelected = function (location, forecast) { 
				            pages.setPage('p_todaydetail', location, forecast);
				        };
				        panel.forecastView = view;
						// refreshButton.on('tap', function () { 
							// view.updateForecast(); 
						// });
					});
					refreshButton.show();
				} else {
					showView('MyWF.view.Splash');
                    refreshButton.hide();
				}
			},
			'p_settings': function () {
                pages.hideToolbars();
                pages.backPage = 'p_splash';
				showView('MyWF.view.Settings', function(view) {
			        view.onBackAction = goToSplashPage;
	        		view.onInfoAction = goToInfoPage;
				});
			},
			'p_locations': function () {
                pages.hideToolbars();
                pages.backPage = 'p_splash';
				showView('MyWF.view.MyLocations', function(view) {
			        view.onBackAction = goToSplashPage;
				});
			},
			'p_addLocation': function () {
                pages.hideToolbars();
                pages.backPage = 'p_splash';
				showView('MyWF.view.AddLocation', function(view) {
			        view.onBackAction = goToSplashPage;
				});
			},
            'p_info': function () {
                pages.hideToolbars();
                pages.backPage = 'p_settings';
				showView('MyWF.view.Info', function(view) {
			        view.onBackAction = goToSettingsPage;
				});
            },
            'p_daydetail': function (location, forecast) {
                pages.showBackToolbar();
                pages.backPage = 'p_splash';
				showView('MyWF.view.ForecastDetail', function(view) {
			        view.renderView(location, forecast);
				});
            },
            'p_todaydetail': function (location, forecast) { 
                pages.showBackToolbar();
                pages.backPage = 'p_splash';
				showView('MyWF.view.ForecastDetail', function(view) {
			        view.renderTodayView(location, forecast);
				});
            },
		};
		
		var quitting = false;
        var onPhoneBackKeyDown = function () {
            if (pages.backPage) {
                pages.setPage(pages.backPage);
                return false;
            }
            if (Ext.os.is('Android')) {
                if (quitting) return;
                quitting = true;
                if (_confirm('Please confirm to quit')) {
                    navigator.app.exitApp();
                }
                window.setTimeout(function(){quitting = false;},300);
                return false;
            };
        };
        if (Ext.os.is('Android')) {
            onAndroidPhoneBackKeyDown = function () { 
                onPhoneBackKeyDown(); 
            };
        };
        
                
        var goToSplashPage = function () { pages.setPage('p_splash'); return false; };
        var goToSettingsPage = function () { pages.setPage('p_settings');  return false; };
        var goToInfoPage = function () { pages.setPage('p_info');  return false; };
        document.addEventListener("backbutton", onPhoneBackKeyDown, false);
        document.addEventListener("menubutton",function(){
            if (pages.currentPage === 'p_settings') return;
            goToSettingsPage();
        }, false);
        
        Ext.Viewport.add({
			xtype: 'container',
			layout: 'fit',
			items: [
				panel,
				mainToolbar,
				backToolbar
			]
		});

        _buttonListenAt('#refreshButton', function () { panel.forecastView.updateForecast(); }, Ext.Viewport);
        _buttonListenAt('#settingsButton', goToSettingsPage, Ext.Viewport);
        _buttonListenAt('#favoritesButton', function () { pages.setPage('p_locations'); }, Ext.Viewport);
        _buttonListenAt('#addLocationButton', function () { pages.setPage('p_addLocation'); }, Ext.Viewport);
        _buttonListenAt('#backToHomeButton', goToSplashPage, Ext.Viewport);
        
        goToSplashPage(); 

	};
	 
	Ext.Loader.setConfig({ 
	    enabled: true, //set false for Synchronous loading message with sencha-touch-debug.js 
	    paths: {
	        'MyWF': 'ui-s'
	    },
	    disableCaching: false
	});

	//Ext.require('Ext.MessageBox');
	Ext.require('Ext.Toolbar');
	Ext.require('Ext.DateExtras');
	Ext.require('Ext.field.Text');
    Ext.require('Ext.field.Select');
    Ext.require('Ext.field.Toggle');
    Ext.require('Ext.field.Search');
	Ext.require('Ext.form.FieldSet');
	Ext.require('Ext.Label');
	Ext.require('Ext.data.Store');
    Ext.require('Ext.dataview.List');
    Ext.require('Ext.TitleBar'); //for select.usePicker: auto/true

	Ext.application({
	    icon: 'images/sym64wwol/wsymbol_0013_sleet_showers.png',
	    glossOnIcon: false,
	    tabletStartupScreen: 'images/sym64wwol/wsymbol_0013_sleet_showers.png',
	    phoneStartupScreen: 'images/sym64wwol/wsymbol_0013_sleet_showers.png',
	    requires: [
            'MyWF.view.Forecast',
            'MyWF.view.ForecastDetail',
	        'MyWF.view.MyLocations',
	        'MyWF.view.AddLocation',
	        'MyWF.view.Settings',
	        'MyWF.view.Splash',
	        'MyWF.view.Info'
	    ],
	    launch: function() {
	        //Phonegap
	        if (typeof (navigator.splashscreen) !== 'undefined') navigator.splashscreen.hide();

			init();
			try {
                var element = document.getElementById('appLoadingIndicator');
                element.parentNode.removeChild(element);
            }
            catch (e) {}
	    }
	});

})();