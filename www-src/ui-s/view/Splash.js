(function() {

	Ext.define('MyWF.view.Splash', {
		extend: 'Ext.Container',
		config: {
			items: [
			{
				xtype: 'fieldset',
				title: 'Simple Weather Forecast',
				centered: true,
				cls: 'myFieldset',  
				instructions: ''
					+ 'Add a location to favorites'
					+ '<br> by the ADD button'
		          	+ '<br> <img style="height: 25px;" src="ui-s/resources/images/add.png">'
					+ '<br><br> choose a favorite location' 
					+ '<br> by the STAR button'
		          	+ '<br> <img style="height: 25px;" src="ui-s/resources/images/favorites.png">'
				,items: []
			}]
		}
	});
	
})();