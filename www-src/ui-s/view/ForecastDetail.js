(function() {

    var renderDetailedForecast = function(location, forecast) {
        this.detailedForecastList.getStore().setData([
//    	{ title: (location.city || location.getString()) + '<br>' + Ext.Date.format(forecast.date, 'l j') },
        { title: (location.city || location.getString())},
        { title: Ext.Date.format(forecast.date, 'l j') },
        { label: ''
//            + '<div class="myVCenterHBox">'
            + '<div>' + forecast.customDescription + '&nbsp;&nbsp;&nbsp;</div>'
            + '<img style="height: 40px;" src="'
            //+ convertImagePath(forecast.iconUrl) + '">' 
            + forecast.iconUrl + '">' 
//        	+ '</div>'
		},
        { label: 'Min ' + forecast.minTemperature + '&deg; - Max '
			+ forecast.maxTemperature + '&deg;'
        },
        { label: 'Precipitations:&nbsp;&nbsp;' + forecast.precipitationMM + '&nbsp;mm' },
        { label: 'Wind:&nbsp;&nbsp;' + forecast.wind.getSpeed(myWF.settings.windMeasureUnit)
			+ '&nbsp;&nbsp;direction: ' + forecast.wind.direction
        }
    ]);}
    ,renderTodayDetailedForecast = function(location, forecast) {
    	this.detailedForecastList.getStore().setData( [
        { title: (location.city || location.getString())},
        { title: Ext.Date.format(forecast.date, 'l j') },
        { label: ''
//            + '<div class="myVCenterHBox">'
            + '<div>' + forecast.customDescription + '&nbsp;&nbsp;&nbsp;</div>'
            + '<img style="height: 40px;" src="'
            //+ convertImagePath(forecast.iconUrl) + '">' 
            + forecast.iconUrl + '">' 
//            + '</div>'
        },
        { label: '<span style="font-size: 1.2em; font-weight: bold;">' + forecast.temperature + '</span>&deg;&nbsp;&nbsp;[Min '
            + forecast.minTemperature + '&deg; - Max '
            + forecast.maxTemperature + '&deg;]'
        },
        { label: 'Precipitations:&nbsp;&nbsp;' + forecast.precipitationMM + ' mm' },
        { label: 'Humidity:&nbsp;&nbsp;' + forecast.humidity + '%' },
        { label: 'Pressure:&nbsp;&nbsp;' + forecast.pressure + ' mbar' },
        { label: 'Clouds:&nbsp;&nbsp;' + forecast.cloudCover + '%' },
        { label: 'Visibility:&nbsp;&nbsp;' + forecast.visibility + '%'},
        { label: 'Wind:  ' + forecast.wind.getSpeed(myWF.settings.windMeasureUnit)
            + '&nbsp;&nbsp; direction ' + forecast.wind.direction
        }
	]);};

    Ext.define('MyWF.view.ForecastDetail', {
        extend: 'Ext.Container',
        initialize: function () {
        	this.detailedForecastList = this.query('#detailedForecast')[0];
        	this.callParent();	
        }
        ,config: {
            layout: 'vbox',
            padding: '10',
            items: [{
            	xtype: 'list'
            	,itemId: 'detailedForecast'
				,flex: 1
	    		,itemTpl: '<div class="myHBox"><div class="myFlex myHBox"></div><span style="font-size: 1.1em; font-weight: bold;">{title}</span>{label}<div class="myFlex"></div></div>'
	    		,store: { fields: ['title', 'label'] }
	    		,disableSelection: true
	    		,scrollable: 'vertical'
    		}]
        }
        ,renderTodayView: function (location, forecast) {
    		renderTodayDetailedForecast.call(this, location, forecast);
        }
        ,renderView: function (location, forecast) {
    		renderDetailedForecast.call(this, location, forecast);
        }
    });

})();
