(function(ns) {

    var _forecastList = ns.forecastList
    	,_myLocations = ns.myLocations 
    	,_currentLocation = _myLocations.currentItem
    	,_injectProperties = ns.util.injectProperties
    	,_isNetworkConnected = ns.util.isNetworkConnected
        ,_getWeekDayName = ns.util.getWeekDayName
        ,_getMonthName = ns.util.getMonthName;
        
    var setRain = function (o, mm) {
        o.precipitationMM = mm + '&nbsp;mm';
        if (mm > 48) {
            o.dropsImage = "images/drops3.png";
        } else if (mm > 16) {
            o.dropsImage = "images/drops2.png";
        } else if (mm > 0) {
            o.dropsImage = "images/drops1.png";
        } else {
            o.dropsImage = "images/drops0.png";
            o.precipitationMM = '';
        }
    };

    var updateAndSave = function(that) {
        if (!_isNetworkConnected()) {
        	that.setError('Off line'); 
        	return;
        }
		that.setMessage('');
        if (!_myLocations.currentItem) {
		    that.setError('Choose a favourite location'); //should never appear
            return;
        }
        
    	var secondsElapsed = ((new Date()).getTime() - _myLocations.currentItem.lastUpdateTime) / 1000; 
        if (secondsElapsed < (3600/4)) {
			that.setMessage('Tap on a day');
        	return;
        }
		that.setMessage('Updating ...');
    	_forecastList.update({query: _myLocations.currentItem.getGeoLocationString()});
    }

    ,mapFunction = function(forecast) {
       var date = forecast.date;
       var o = {
            image: forecast.iconUrl,
            maxTemp: ns.settings.convertTemperature(forecast.maxTemperature),
            minTemp: ns.settings.convertTemperature(forecast.minTemperature),
            monthDay: date.getDate(),
            monthName: _getMonthName(date.getMonth()),
            weekDayName: _getWeekDayName(date.getDay()),
            forecast: forecast
        };
        setRain(o, forecast.precipitationMM);
        return o;
    } 
    ,daysTemplateStrings = [
    '<div class="myRow">',
    '<div class="myHBox" style="margin: 0px auto; max-width: 450px;">',
        '<img style="height: 2.5em; width: 2.5em;" src="{image}">',
        '<div class="myFlex"></div>',
        '<div style="text-align:center; font-size: 0.85em;">',
            '{weekDayName}<br>',
            '{monthDay}&nbsp;{monthName}',
        '</div>',
        '<div class="myFlex"></div>',
        '<div>',
            '<div class="myHBox" style="-webkit-box-pack: start; margin: 0 0 0.2em 0;">',
                '<div><img  style="height: 1.1em;" src="{dropsImage}"></div>',
                '<div style="height: 0.7em; margin: 0 0 0.5em 0.3em;">{precipitationMM}</div>',
            '</div>',
            '<img style="height: 1.3em; margin: 0 3px -3px 0;" src="images/lowtemp.png">',
            '<span style="font-size: 1.3em;">{minTemp}&deg; </span>',
            '<img style="height: 1.3em; margin: 0 3px -3px 0;" src="images/hitemp.png">',
            '<span style="font-size: 1.3em;">{maxTemp}&deg; </span>',
        '</div>',
    '</div>',
    '</div>'
    ] 

    ,composeTodayObject = function(location, forecast) {
        var date = forecast.date;
        var time = forecast.observationTime;
        var mm = forecast.precipitationMM;
        var soup = {
            desc: location.getString(),
            image: forecast.iconUrl,
            weekDayName: _getWeekDayName(forecast.date.getDay()),
            monthDay: date.getDate(),
            monthName: _getMonthName(date.getMonth()).substr(0,3),
            time: (time.getHours()).toString() + ":" + (time.getMinutes()).toString(),
            windSpeed: forecast.wind.getSpeed(ns.settings.windMeasureUnit),
            windDirection: forecast.wind.direction,
            weatherDescription: forecast.weatherDescription,
            maxTemp: ns.settings.convertTemperature(forecast.maxTemperature),
            minTemp: ns.settings.convertTemperature(forecast.minTemperature),
            currentTemp: ns.settings.convertTemperature(forecast.temperature)
        };
        setRain(soup, mm);
        _injectProperties(location, soup);
        //soup.description = location.getLongString();
        return soup;
    }
    ,todayTemplate = new Ext.Template([
    '<div class="myHead">',
    '<div class="myVBox" style="margin: 0 auto; max-width: 450px;">',
        '<div>{desc}</div>',
        '<div class="myHBox" style="-webkit-box-align: start;">',
            '<div class="myVBox" style="-webkit-box-align: end; margin: 0 0.5em 0 0;">',
                  '<img style="height: 2.5em; width: 2.5em;" src="{image}">', 
                  '<div class="myHBox" style="-webkit-box-pack: start; margin: -10px 0 0.2em 0;">',
                    '<div><img style="height: 0.8em;" src="{dropsImage}"></div>',
                    '<div style="font-size: 0.75em; margin: 0 0 0 0.3em;">{precipitationMM}</div>',
                '</div>',   
            '</div>',
            '<div>',
                '<div>',
                    '<span style="font-size: 1.2em;">{currentTemp}&deg;C&nbsp;&nbsp;</span>',
                    '<img style="height: 1.1em; margin: 0 3px -3px 0;" src="images/lowtemp.png">',
                    '<span style="font-size: 1.1em; margin-bottom: 0.2em;">{minTemp}&deg;</span>',
                    '<img style="height: 1.1em; margin: 0 3px -3px 3px;" src="images/hitemp.png">',
                    '<span style="font-size: 1.1em; margin-bottom: 0.2em;">{maxTemp}&deg;</span>',
                '</div>',
                '<div style="font-size: 0.7em;">',
                    '{weekDayName}&nbsp;{monthDay}&nbsp;{monthName}&nbsp;{time}',
                '</div>',
                '<div style="font-size: 0.7em; max-width: 30ex;">',
                    '{weatherDescription}',
                '</div>',
            '</div>',
        '</div>',
    '</div>',
    '</div>'
    ])

    ,renderData = function(that) {
        var list = that.daysForecasts;
        var store = list.getStore();
    	//:todo find a better place for the following if
		//_myLocations.currentItem.lastUpdateTime = 0;
    	if (_myLocations.previousItem !== _myLocations.currentItem) {
    		_forecastList.data = {};
    		_forecastList.save();
    		_myLocations.currentItem.lastUpdateTime = 0;
    	}

        try {
            list.setHtml('');
	        if (!_forecastList.data 
	            || (_forecastList.data === {})
	            || (!_forecastList.data.list) 
	            || (_forecastList.data.list.length === 0)) {
	        	store.setData([]);
                list.setHtml('<br><div class="myHBox packCenter">' + _myLocations.currentItem.getString() + '</div><br>');
                list.refresh();
	            return;
	        }
	        that.todayForecast.setHtml(todayTemplate.apply(composeTodayObject(
	        	_myLocations.currentItem, _forecastList.data.currentConditions
	        )));
	        store.setData(ns.util.map(_forecastList.data.list, mapFunction, 1));
            window.setTimeout(function() {list.refresh();},1000);
            list.refresh();
        } catch (e) { 
            ns.util.logSet('Forecast rendering', e.toString())
            if (ns.isDebug) throw e;
        	list.setEmptyText(e.toString());
            list.refresh();
        };
    };

    todayTemplate.compile();

    Ext.define('MyWF.view.Forecast', {
        extend: 'Ext.Container'
        ,onTodaySelected: function (location, forecast) {}
        ,onDaySelected: function (location, forecast) {}
		,initialize: function () {
			var that = this;
            var message = this.query('#statusMessage')[0];
			this.daysForecasts = this.query('#daysForecasts')[0];                
			this.todayForecast = this.query('#todayForecast')[0];                
			this.daysForecasts.setItemTpl((new Ext.Template(daysTemplateStrings)).compile());
            this.statusMessage;

            this.setError = function(messageString) {
                setMessage(messageString, 'error');
            };
            this.setMessage = function(messageString) {
                setMessage(messageString, '');
            };
            var setMessage = function(messageString, cls) {
                message.hide(); 
                message.setCls(cls);
                if (messageString) {
                    message.setHtml('&nbsp;'+messageString+'&nbsp;');
                    message.show(); 
                }
            };

            this.daysForecasts.on('select', function (dataView, record, eOpts) {
                ns.util.vibrate();
                that.onDaySelected(_myLocations.currentItem, record.data.forecast);
            });
            // this.daysForecasts.on('itemtouchstart', function () {
                // ns.util.vibrate();
            // });

            this.todayForecast.on('touchstart', function () {
                that.todayForecast.setCls('mySelected');
                // ns.util.vibrate();
            }, that, {element: 'element'});
            this.todayForecast.on('touchend', function () {
                that.todayForecast.setCls('myUnSelected');
            }, that, {element: 'element'});
			this.todayForecast.on('tap', function () {
                ns.util.vibrate();
			    that.todayForecast.setCls('myUnSelected');
	    		that.onTodaySelected(_myLocations.currentItem, _forecastList.data.currentConditions); 
			}, that, {element: 'element'});

	        this.updateForecast = function () { updateAndSave(that); };
			
	        _forecastList.onSuccess = function() {   
				_myLocations.currentItem.lastUpdateTime = new Date().getTime();
	            _myLocations.save();
		        _forecastList.save();
                _myLocations.previousItem = _myLocations.currentItem;
	            renderData(that);
		        that.setMessage('Tap on a day');
	        };
	        _forecastList.onFail = function() {
	            renderData(that);
                if (_forecastList.status.isTimeout()) {
                    that.setError('Request Timeout');
                } else {
                    that.setError('Update error');
                }
	        };

			this.callParent();		
		}
        ,config: {
            layout: 'vbox'
            ,padding: '5'
            ,listeners: [
            {
                event: 'show', 
                fn: function() {
		            renderData(this); //render last saved
                    updateAndSave(this);
                }
            }],
            items: [{
				xtype: 'container'
		        ,itemId: 'todayForecast'
		    },{
		        xtype: 'list',
		        itemId: 'daysForecasts',
		        scrollable: true,
		        flex: 1,
		        store: { fields: [
		            'image', 'maxTemp', 'minTemp', 'weekDayName', 
		            'monthDay', 'monthName', 'precipitationMM',
		            'forecast', 'dropsImage'
		        ]}
		    },{ 
				xtype: 'container' 
            	,layout: 'hbox'
                ,items: [
                	{ xtype: 'spacer' } 
                    ,{
                        xtype: 'label'
                        ,itemId: 'statusMessage'
                        ,align: 'center'
                    }
				    ,{ xtype: 'spacer' }
				]
			}]
        }
    });

})(myWF);
