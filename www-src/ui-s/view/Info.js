(function() {

    var _buttonListenAt = myWF.sencha.buttonListenAt;

    var createToolbar = function () { 
    	return Ext.create('Ext.Toolbar', {               
        xtype: 'toolbar',
        docked: 'bottom',
        layout: { pack: 'end' },
        items: [{ 
            iconMask: true, ui: 'normal', iconCls: 'reply',
            itemId: 'backButton'
        }]
    })};
    
	Ext.define('MyWF.view.Info', {
		extend: 'Ext.Container'
		,initialize: function ()
		{
			this.setItems([createToolbar()]);

            _buttonListenAt('#backButton', 'onBackAction', this);
			this.callParent();
		}
        ,onBackAction: function () { console.log('back from info'); }
		,config: {
	        // listeners: [{
	            // delegate: "#backButton",
	            // event: 'tap',
	            // fn: 'onBackAction'
	        // }],
			items: [
            {
                xtype: 'fieldset',
                title: 'Credits',
                //align: 'center',
                centered: true,
                instructions: [
                    'Simple weather forecast<br>version 1.1 rc1'
                    ,'<br><br>Powered by <a href="http://www.worldweatheronline.com/" title="Free local weather content provider" target="_blank">World Weather Online</a>'
                    ,'<br>Free local weather content provider'
                    ,'<br><br>Locantions lookup by Yahoo'
                    ,'<br> <a href="http://developer.yahoo.com/geo/placefinder/">Yahoo Developer API</a>'
                ].join(''), 
                items: []
            }]
		}
	});
	
})();