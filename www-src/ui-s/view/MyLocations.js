(function () {

    var _map = myWF.util.map,
    	_myLocations = myWF.myLocations,
        _alert = myWF.messages.alert,
        _confirm= myWF.messages.confirm,
        _buttonListenAt = myWF.sencha.buttonListenAt,

        setCurrentLocation = function (location) {
            _myLocations.add(_myLocations.currentItem); //save modifications (last update time)
            _myLocations.currentItem = location; 
            _myLocations.save();
        },  
        removeLocation = function (location) {
            var current = _myLocations.currentItem;
            if (location && current &&
                    (location.identity() === current.identity())) {
                _myLocations.currentItem = null;    
            }
            _myLocations.remove(location).save();
        };

    var confirmRemoveLocation = function (that) {
            var list = that.getComponent('myLocationsExtList');
            if (list.getSelectionCount() === 0) {
                _alert("Select a location"); 
                return;
            }
            if (_confirm('Remove the location ?')) {
                removeLocation(list.getSelection()[0].data.object);
                loadData(that);
            };
        },
        currentLocationSelected = function (that) {
            var list = that.getComponent('myLocationsExtList');
            
            if (list.getSelectionCount() === 0) {
                // Ext.Msg.alert("", "Select a location"); 
                _alert("Select a location"); 
                return false;
            }
            setCurrentLocation(list.getSelection()[0].data.object);
            return true;
        },
        loadData = function (that) {
            var list = that.getComponent('myLocationsExtList');
            var store = list.getStore();
            store.setData(_map(_myLocations.toArray(), function(element) {
                return {
                    string: element.getLongString(),
                    object: element
                };
            }));
            list.refresh();
        };
        
    var createList = function () { 
        var store = Ext.create('Ext.data.Store', {
            fields: ['string', 'object']
        });

        return Ext.create('Ext.dataview.List', {
                xtype: 'list',
                itemId: 'myLocationsExtList',
                scrollable: true,
                flex: 1,
                store: store,
                itemTpl: '{string}',
                //centered: true,
                emptyText: '<div class="myEmptyTextContainer"><div class="myEmptyText">'
                    + 'You still have no favorite locations.'
                    + '<br>Please go back and add a location'
                    + '<br><br>by the ADD button'
                    + '<br><img style="height: 25px;" src="ui-s/resources/images/add.png">'
                    + '</div></div>'
    })},
    createToolbar = function (that) { 
        return Ext.create('Ext.Toolbar', {               
            xtype: 'toolbar',
            docked: 'bottom',
            layout: { pack: 'center' },
            items: [
                { 
                    iconMask: true, ui: 'normal', iconCls: 'trash', 
                    itemId: 'deleteButton'
                    // ,listeners: { 
                         // tap: function () { confirmRemoveLocation(that); }
                     // }
                }
                ,{ xtype: 'spacer' }
                ,{ 
                    iconMask: true, ui: 'normal', iconCls: 'favorites', 
                    itemId: 'selectButton'
                }
                ,{ xtype: 'spacer' }
                ,{ 
                    iconMask: true, ui: 'normal', iconCls: 'reply',
                    itemId: 'backButton'
                }
            ]
    })};
        
    Ext.define('MyWF.view.MyLocations', {
        extend: 'Ext.Container',
        initialize: function ()
        {
            that = this;
            this.setItems([createToolbar(this), createList()]);

            _buttonListenAt('#backButton', 'onBackAction', that);
            _buttonListenAt('#selectButton', 'onSelectAction', that);
            _buttonListenAt('#deleteButton', 'onDeleteAction', that);
            this.callParent();
        },
        items: [],
        config: {
            layout: 'vbox',
            padding: '10',
            listeners: [
            /*
             * listeners :{
                afterrender : function(cmp){
                   cmp.refresh();
                }
            }
             */
            // {
                // delegate: "#backButton",
                // event: 'tap',
                // fn: 'onBackAction'
            // }
            //,
            // {
                // delegate: "#selectButton",
                // event: 'tap',
                // fn: 'onSelectAction'
            // }
            // ,
            {
                event: 'show',
                fn: function (that, eOpts) { loadData(that); }
            }]
        },
        onBackAction: function () { 
            console.log('onBackaction in MyLocations.js'); 
        }
        ,onSelectAction: function () { 
            if (currentLocationSelected(this)) { this.onBackAction(); }
        }
        ,onDeleteAction: function () { 
            confirmRemoveLocation(this);
        }
    
    });
    
}) ();