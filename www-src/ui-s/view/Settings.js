(function(ns) {

/*
	var getStatus = function (){
		var statusHtml = '';
		if (myWF.util.logHas('infoMessage')) {
			statusHtml += myWF.util.logGet('infoMessage') + '<br>';
		}
		if (myWF.forecastList.status.error) {
			statusHtml += 'Forecast update error: ' + myWF.forecastList.status.errorResultMessage + '<br>';
		}
		if (myWF.locationList.status.error) {
			statusHtml += 'Location lookup error: ' + myWF.locationList.status.errorResultMessage + '<br>';
		}
		if (typeof (device) === 'undefined') { return statusHtml; };
		try {
		    statusHtml += "<b>Connection</b><br>" + myWF.util.networkStatus()
		    	+ '<br><br><b>Device</b><br>'
		    	+ 'Name: ' + device.name + '<br>'
		    	+ "UUID: " + device.uuid + '<br>'
		    	+ "Platform: " + device.platform + ", " + device.version + '<br>'
		    	+ "Screen: " + screen.width + ' x ' + screen.height + ', '+ screen.colorDepth + ' colors';
		} catch (e) {};  
		return statusHtml;
	};
*/	
    var _buttonListenAt = ns.sencha.buttonListenAt;

    var getStatus = function (){
        var a = myWF.util.logToArray();
        //if (a.length === 0) return '';
        a.unshift('Network: ' + ns.util.networkStatus());
        a.join('<br>');
        return a;
    };
	var setSettingValue = function(component, query, value) {
		var c = component.query(query);
		if (c && value && (c.length > 0)) {
			c[0].setValue(value);
		}
	};
	var setSettingHtml = function(component, query, value) {
		var c = component.query(query);
		if (c && value && (c.length > 0)) {
			c[0].setHtml(value);
		}
	};
    var createToolbar = function () { 
    	return Ext.create('Ext.Toolbar', {               
        xtype: 'toolbar',
        docked: 'bottom',
        layout: { pack: 'center' },
        items: [
			{ 
				iconMask: true, ui: 'normal', iconCls: 'info', 
                itemId: 'infoButton'
			}
            ,{ xtype: 'spacer' }
            ,{ 
                iconMask: true, ui: 'normal', iconCls: 'reply',
                itemId: 'backButton'
            }
        ]
    })};

	Ext.define('MyWF.view.Settings', {
		extend: 'Ext.Container',
		initialize: function ()
		{
			this.setItems([createToolbar()]);
            this.status = this.query('#status')[0];
            //this.statusContainer = this.query('#statusContainer')[0];

            _buttonListenAt('#backButton', 'onBackAction', this);
            _buttonListenAt('#infoButton', 'onInfoAction', this);
            
			this.callParent();
		},
		config : {
			layout : 'vbox',
			// padding : '5 0 0 0',
			scrollable: {
		        direction: 'vertical'
		    },	
	        onBackAction: function () { console.log('back'); }
	        ,onInfoAction: function () { console.log('info'); }
            ,listeners: [
            //{
                // delegate: "#backButton",
                // event: 'tap',
                // fn: 'onBackAction'
            // }
            // ,{
                // delegate: "#infoButton",
                // event: 'tap',
                // fn: 'onInfoAction'
            // },
            {
                event: 'show',
                fn: function(component, eOpts) {
					setSettingValue(component, 'selectfield[name=windMU]',
							ns.settings.windMeasureUnit);
                    setSettingValue(component,
                            'selectfield[name=temperatureMU]',
                            ns.settings.temperatureMeasureUnit);
                    setSettingValue(component,
                            'togglefield[name=vibrate]',
                            ns.settings.vibrate);
					var status = getStatus();
                    this.status.setHtml(status);        
					// if (status) {
					    // this.statusContainer.show();
                        // this.status.setHtml(status);        
					// } else {
                        // this.statusContainer.hide();        
					// }		
				}
			}],
			items : [{
				xtype : 'fieldset',
				title : 'Settings',
                items : [{
    				xtype : 'selectfield',
    				name : 'temperatureMU',
    				label : 'Temperature',
    				labelAlign: 'top',
    				usePicker: true,
    				listeners : {
    					change : function(selectField, newData, oldData, eOpts) {
    						ns.settings.temperatureMeasureUnit = newData;
    						ns.settings.save();
    					}
    				},
					options : [{
								text : 'Celsius',
								value : 'C'
							}, {
								text : 'Farenheit',
								value : 'F'
							},]
				}, {
					xtype : 'selectfield',
					name : 'windMU',
					label : 'Wind speed',
					labelAlign : 'top',
                    usePicker: true,
					listeners : {
						change : function(selectField, newData, oldData, eOpts) {
							ns.settings.windMeasureUnit = newData;
							ns.settings.save();
						}
					},
					options : [{
								text : 'Kilometers per hour',
								value : ns.Wind.KILOMETERS_PER_HOUR
							}, {
								text : 'Meters per second',
								value : ns.Wind.METERS_PER_SECOND
							}, {
								text : 'Miles per hour',
								value : ns.Wind.MILES_PER_HOUR
							}, {
								text : 'Knots',
								value : ns.Wind.KNOTS
							}]
				}, {
                xtype : 'togglefield',
                    name : 'vibrate',
                    label : 'Vibration',
                    labelAlign: 'top',
                    listeners : {
                        change : function(toggleField, newValue) {
                            ns.settings.vibrate = toggleField.getValue() !== 0;
                            ns.settings.save();
                        }
                    }
                }
            ]}
                /*, {
                       xtype : 'label',
                       padding: '10',
                       itemId: 'status'
                    }
                */     				
            ,{
				xtype : 'fieldset',
				title : 'Status',
				margin: '-20 0 0 0',
                itemId: 'statusContainer',
				items : [{
					xtype : 'label',
					padding: '10',
					itemId: 'status'
				}]
			}]
	}
	});

})(myWF);	