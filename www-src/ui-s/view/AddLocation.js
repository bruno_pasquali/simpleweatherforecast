(function() {

    var _map = myWF.util.map,
    	_locationsService = myWF.locationList,
    	_myLocations = myWF.myLocations,
    	_isNetworkConnected = myWF.util.isNetworkConnected,
    	_alert = myWF.messages.alert,
        _confirm= myWF.messages.confirm,
        _buttonListenAt = myWF.sencha.buttonListenAt,
        
        mapFunction = function(element) {
            return {
                string: element.getString(),
                object: element
            };
        },
        addFavoriteLocation = function (location) {
            _myLocations.add(location).save();
            // if (!_myLocations.currentItem) {
                _myLocations.currentItem = location;    
            // } 
        };
        
        // var minTime = 1000;
        // var msElapsed = ((new Date()).getTime() - that.lastUpdateTime); 
        // if (msElapsed < minTime) {
            // clearTimeout(that.updateTimer);
            // that.updateTimer = setTimeout(function() {fetchList(that)}, minTime+100);
            // return;
        // }
        // that.lastUpdateTime = msElapsed;
    var clearListForNoAddress = function (that) {
        if (!that.address) {
            that.resetData();
            if (that.country) {
                that.setMessage('Input An Address');
            }   
            return true;
        }
        return false;
    }
    var fetchList = function (that) {
        clearTimeout(that.fetchListTimer);
        that.setMessage('');
        if (clearListForNoAddress(that)) return;
        if (!_isNetworkConnected()) {
            that.setMessage('Off line'); 
            return;
        }    
        //var query = that.address + (that.country ? (',' + that.country) : '');
        that.fetchListTimer = setTimeout(function() { _locationsService.update({
            //todo: demetra
            by: _locationsService.serviceAdapter.byAddressAndCountry,
            address: that.address,
            country: that.country
        })}, 500);
    }
    
    ,confirmSelectedLocation = function (that) {
        var selection = that.getFirstSelected();
        if (!selection) {
            //Ext.Msg.alert('', "Select a location"); 
            _alert('Select a location');
            return false;
        }
        if (_confirm('Save the location ?')) {
            that.locationsList.deselectAll();
            addFavoriteLocation(selection);
        }
        // Ext.Msg.confirm("", "Save the location ?", function (btn) {
            // if (btn === 'yes') addFavoriteLocation(selection);
        // });
        return true;
    }
    ,createItems = function () { 
	    var store = Ext.create('Ext.data.Store', {
	            fields: ['string', 'object']
	    });
        return [
	        Ext.create('Ext.Toolbar', {               
	            docked: 'bottom',
	            layout: { pack: 'center' },
	            items: [
	                { 
	                    //iconMask: true, ui: 'normal', iconCls: 'add', 
	                    text: 'GPS'
	                    ,itemId: 'gpsButton'
	                }
	                ,{ xtype: 'spacer' }
	                ,{ 
	                    iconMask: true, ui: 'normal', iconCls: 'add', 
	                    itemId: 'selectButton'
	                }
	                ,{ xtype: 'spacer' }
	                ,{ 
	                    iconMask: true, ui: 'normal', iconCls: 'reply',
	                    itemId: 'backButton'
	                }
	            ]
	    	})
	    	,Ext.create('Ext.Container', {
	            	layout: 'hbox'
                    ,items: [
                    	{ xtype: 'spacer' } 
                    	,{
							xtype: 'label'
	        				,itemId: 'errorMessageExtLabel'
	        				,style: {'color': 'white', backgroundColor: 'red'} 

	        			}	
						,{ xtype: 'spacer' }]
				}
	    	)
	    	,Ext.create('Ext.dataview.List', {
                xtype: 'list',
                itemId: 'searchedLocationsExtList',
                scrollable: true,
                flex: 1,
                store: store,
                itemTpl: '{string}'
       })];        
    };
    
    Ext.define('MyWF.view.AddLocation', {
        extend: 'Ext.Container',
        initialize: function ()
        {
            var that = this;
            this.fetchListTimer = 0;
            //this.lastUpdateTime = 0;
            this.country = '';
            this.address = '';
            //this.isByGPS = false;
            this.setItems(createItems());
            this.errorMessage = this.query('#errorMessageExtLabel')[0];
            this.countryText = this.query('#countryExtTextField')[0];                
            this.addressText = this.query('#addressExtTextField')[0];                
            this.locationsList = this.getComponent('searchedLocationsExtList');                

            _buttonListenAt('#backButton', 'onBackAction', that);
            _buttonListenAt('#selectButton', 'onSelectAction', that);
            _buttonListenAt('#gpsButton', 'onGpsSearchAction', that);
            
            var message = this.errorMessage;
            this.setMessage = function(messageString) {
                if (!messageString) { 
                    message.setHtml('');
                    message.hide(); 
                    return;
                }
                message.setHtml('&nbsp;'+messageString+'&nbsp;');
                message.show(); 
            };
			var store = this.locationsList.getStore();
			this.setData = function (data) { store.setData(data) };
			this.resetData = function () {
			    store.setData([]);
			    that.locationsList.deselectAll();
			};

            this    .getFirstSelected = function () {
                if (that.locationsList.getSelectionCount() > 0) {
                    return that.locationsList.getSelection()[0].data.object;
                }    
                if (store.getCount() === 1) {
                    return store.getData().all[0].data.object;    
                }
                return null;
            }			
            _locationsService.onSuccess = function(objParameters) {
		        that.setMessage('');
		        if ((objParameters.by === _locationsService.serviceAdapter.byLatLong) 
		            || !clearListForNoAddress(that)) {
                    that.locationsList.deselectAll();
                    that.setData(myWF.util.map(_locationsService.data.list, mapFunction));
                }    
            };
            _locationsService.onFail = function(objOptions) {
                if (_locationsService.status.emptyData()) { 
                    that.setMessage('No locations found');
                    that.resetData();
                } else if (_locationsService.status.isTimeout()) {
                    that.setMessage('Request Timeout');
                } else {
                    that.setMessage('Update error');
                }
            };

            this.callParent();
        },
        config: {
            layout: 'vbox',
            padding: '0 20 20 20',
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Search',
                    items: [{
                    	xtype: 'searchfield',
			            placeHolder: 'Address',
			            itemId: 'addressExtTextField',
                    }
                    ,{
                    	xtype: 'searchfield',
			            placeHolder: 'Country',
			            itemId: 'countryExtTextField',
                    }]			 
                }
            ]
            ,listeners: [
                // {
                    // delegate: "#backButton",
                    // event: 'tap',
                    // fn: 'onBackAction'
                // }
                // ,{
                    // delegate: "#gpsButton",
                    // event: 'tap',
                    // fn: 'onGpsSearchAction'
                // }
                // ,{
                    // delegate: "#selectButton",
                    // event: 'tap',
                    // fn: 'onSelectAction'
                // },
	            {
	                delegate: "#addressExtTextField",
	                event: 'keyup',
	                fn: 'onAddressKeyUp'
	            }
	            ,{
	                delegate: "#countryExtTextField",
	                event: 'keyup',
	                fn: 'onCountryKeyUp'
	            }
                ,{
                    event: 'show',
                    fn: function() {
				        this.addressText.focus();                
                    }
                }
            ]
        }
        ,onGpsSearchAction: function () {
            this.setMessage('Updating ...');
            //this.isByGPS = true;
			myWF.util.getGPSPosition(function (position) {
				//:todo the query parameter is service specific, 
				//  in case of service change use an object parameter for GeoAjaxList
                //    position.coords.latitude + '+' + position.coords.longitude + '&gflags=R'
		        _locationsService.update({
		                  //todo: demetra
                        by: _locationsService.serviceAdapter.byLatLong,
		                latitude: position.coords.latitude,
		                longitude: position.coords.longitude
		        });
			}, _locationsService.onFail);
		}
		,onAddressKeyUp: function (text, e, opts) {
            if (text.getValue() !== this.address) {
                this.address = text.getValue(); 
                fetchList(this); 
            }
	 	}
        ,onCountryKeyUp: function (text, e, opts) {
            if (text.getValue() !== this.country) {
                this.country = text.getValue();
        	    fetchList(this);
        	} 
		}
        ,onBackAction: function () { console.log('back'); }
        ,onSelectAction: function () { 
            confirmSelectedLocation(this); 
         }   
    });
        
}) ();
