# Get the directory that this configuration file exists in
dir = File.dirname(__FILE__)

# Load the sencha-touch framework automatically.
#load File.join(dir, '..', '..', 'sdk', 'resources', 'themes')
#cmd3
load File.join(dir, '..', '..', 'touch', 'resources', 'themes')

# Compass configurations
sass_path = dir
css_path = File.join(dir, "..", "css")

# Require any additional compass plugins here.
images_dir = File.join(dir, "..", "images")

#:nested :expanded :compact :compressed.
output_style = :compressed 
#output_style = :expanded 

environment = :production
#environment = :development
