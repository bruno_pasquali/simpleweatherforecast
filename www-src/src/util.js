var myWF = (function(ns) {
	var __locale = 'en';
	
	var logBag = {};
	var states = {};
//	if (typeof (Connection) !== 'undefined') {
//	  states[Connection.UNKNOWN]  = 'Unknown';
//	  states[Connection.ETHERNET] = 'Ethernet';
//	  states[Connection.WIFI]     = 'WiFi';
//	  states[Connection.CELL_2G]  = 'Mobile 2G';
//	  states[Connection.CELL_3G]  = 'Mobile 3G';
//	  states[Connection.CELL_4G]  = 'Mobile 4G';
//	  states[Connection.NONE]     = 'None';
//	} 
	var _toString = Object.prototype.toString;
	var _parseJSON = JSON.parse;
	  
	//mapped on jquery
	//var _isFunction = $.isFunction;
	var _isFunction = function (f) { 
		return _toString.call( f ) === '[object Function]';  
	};
	var _isObject = function (o) {
        return _toString.call( o ) === '[object Object]';
    };    
    var _hasOwnProperty = Object.hasOwnProperty;
	  
	var weekday = [
	    "Sunday",
        "Monday",
    	"Tuesday",
    	"Wednesday",
    	"Thursday",
    	"Friday",
    	"Saturday"
    ];

    var monthNames = [ 
        "January", "February", "March", 
        "April", "May", "June",
        "July", "August", "September", 
        "October", "November", "December" 
    ];

	//from http://www.quirksmode.org/js/xmlhttp.html
    var XMLHttpFactories = [
        function () {return new XMLHttpRequest()},
        function () {return new ActiveXObject("Msxml2.XMLHTTP")},
        function () {return new ActiveXObject("Msxml3.XMLHTTP")},
        function () {return new ActiveXObject("Microsoft.XMLHTTP")}
    ];
    function createXMLHTTPObject() {
        var xmlhttp = false;
        for (var i=0;i<XMLHttpFactories.length;i++) {
            try {
                xmlhttp = XMLHttpFactories[i]();
            }
            catch (e) {
                continue;
            }
            break;
        }
        return xmlhttp;
    }
	var _sendRequest = (function () {
		var sendRequest = function (url, onSuccess, onFail, objOptions, postData) {
            var req = createXMLHTTPObject();
		    if (!req) return;
		    var method = (postData) ? "POST" : "GET";
		    req.open(method,url,true);
		    //req.setRequestHeader('User-Agent','XML  HTTP/1.0');
		    var timer = setTimeout(function() {
		    	req.abort()
		        onFail('', 'Timeout', objOptions);
		    },10000);
		    if (postData)
		        req.setRequestHeader('Content-type','application/x-www-form-urlencoded; charset=UTF-8');
		    req.onreadystatechange = function () {
		        if (req.readyState != 4) return;
                clearTimeout(timer);
		        if (req.status != 200) { // && req.status != 304) {
			        onFail(req.responseText, req.statusText, objOptions);
		            return;
		        }
	        	var dataType = req.getResponseHeader('Content-Type');
	        	if (dataType && (dataType.indexOf('json') !== -1)) {
		        	onSuccess(_parseJSON(req.responseText), req.statusText, objOptions);
		        } else {
		        	onSuccess(req.response, req.statusText, objOptions);
		        }	
		    }
		    if (req.readyState == 4) return;
		    req.send(postData);
		};
		return sendRequest;
	})();

    //Phonegap
    ns.messages = {};
    ns.messages.alert = function (message) {
        if (typeof (navigator.notification) === 'undefined') { 
            window.alert(message);
         } else {
            navigator.notification.alert(message);
         }   
    };
    //Phonegap
    ns.messages.confirm = function (message) {
        // if (typeof (navigator.notification) === 'undefined') { 
            return window.confirm(message);
         // } else {
          // var result = false;  
          // navigator.notification.confirm(message, function(button) {
              // result = (button === 1);
              // console.log('Confirm button: ' + button);
          // });    
          // return result;
         // }   
    };

   //Phonegap 2.2.0 bug
    var _hasConnectionType = function () {
        return ((typeof (navigator.connection) !== 'undefined'));
            //|| (typeof (navigator.network.connection) !== 'undefined'));
    };
    var _connectionType = function () {
        return (navigator.connection.type); // || navigator.network.connection);
    };

    ns.util = {
    validateConstructorCall: function(objThis, objType) {
      if (!(objThis instanceof objType)) {
        throw new Error('Contructor called without new');
      }
    },
    validateCreatorCall: function(objThis, objType) {
      if (objThis instanceof objType) {
        throw new Error('Creator called with new');
      }
    },
    
	trim: function(s) {
	  var str = s.replace(/^\s\s*/, ''),
	      ws = /\s/,
	      i = str.length;
	  while (ws.test(str.charAt(--i)));
	  return str.slice(0, i + 1);
	},
    map: function(array, callback, startIndex) {
        var a = [];
        var i = startIndex || 0;
        for (; i < array.length; i++) {
          a.push(callback(array[i], i, array));
        }
        return a;
    },
	iterate: function(array, callback) {
	  for (var i = 0; i < array.length; i++) {
	    callback(array[i], i, array);
	  }
	  return array;
	},
    mapObjProps: function(obj, callback) {
      a = [];          
      if (!obj) { return a; }
      for (var p in obj) {
        if (_hasOwnProperty.call(obj, p) && !_isFunction(p)) {
            a.push(callback(p, obj[p], obj));
        }
      }
      return a;
    },
    decRound: function(number, places) {
    	var multiplier = Math.pow(10, places);
    	return Math.round(number * multiplier) / multiplier;
    },

    config: {
      updateMethod: {
        type: 1,
        GEOLOCATION: 1,
        CITYNAME: 2
      }
    },
    setLocale: function(locale) {
      __locale = locale;
    },
    getLocale: function() {
      return (navigator.language.substr(0, 2) || __locale);
    },

    assingPropertyValues: function(fromObj, toObj) {
      if (!fromObj) {
        return;
      }
      for (var p in toObj) {
        if (_hasOwnProperty.call(fromObj, p) 
            && _hasOwnProperty.call(toObj, p) 
            && !_isFunction(fromObj[p]) 
            && !_isFunction(toObj[p])) {
          toObj[p] = fromObj[p];
        }
      }
    },
    injectProperties: function(fromObj, toObj) {
      if (!toObj) {
        return;
      }
      for (var p in fromObj) {
        if (_hasOwnProperty.call(fromObj, p) && !_isFunction(p)) {
          toObj[p] = fromObj[p];
        }
      }
    },

    logSet: function(key, value) {
      logBag[key] = value;
    },
    logToArray: function() {
        return ns.util.mapObjProps(logBag, function(key, value) {
          return (key + ': ' + value);
        });
    },
    logGet: function(key) {
      return logBag[key]; 
    },
    logHas: function(key) {
      return (typeof (logBag[key]) !== 'undefined'); 
    },
    log: function(s) {
      if (console) {
        console.log(s);
      }
    },

    parseJSON: _parseJSON,
    toJSON: JSON.stringify,
    isArray:  Array.isArray || function (obj) {
		return obj && _toString.call(obj) === '[object Array]';  
	},
	ajax: _sendRequest,    
    // mapped on jquery
    // parseJSON: $.parseJSON,
    // toJSON: $.toJSON,
    // isArray: $.isArray,
    /*
    ajax: function(inUrl, onSuccess, onFail) {
      try {
        $.ajax({
          url: inUrl,
          success: function(data, textStatus, jqXHR) {
            if (onSuccess) {
              onSuccess(data, textStatus);
            }
          },
          error: function(jqXHR, textStatus, exception) {
            if (onFail) {
              onFail(textStatus, exception);
            }
          }
        });
        $.get();
      }
      catch (e) {
        console.log(e); 
      }
    },
    */
    getWeekDayName: function (day) {
        if ((day < 0) || (day > 6)) return ''; 
        return weekday[day];
    },
    getMonthName: function (month) {
        if ((month < 0) || (month > 11)) return ''; 
        return monthNames[month];
    },

    //Phonegap
    networkStatus: function () {
        if (_hasConnectionType()) {
           states[Connection.UNKNOWN]  = 'Unknown';
           states[Connection.ETHERNET] = 'Ethernet';
           states[Connection.WIFI]     = 'WiFi';
           states[Connection.CELL_2G]  = 'Mobile 2G';
           states[Connection.CELL_3G]  = 'Mobile 3G';
           states[Connection.CELL_4G]  = 'Mobile 4G';
           states[Connection.NONE]     = 'None';
           //return (states[navigator.connection.type] || 'unknown');
           return (states[_connectionType()] || _connectionType());
        } 
        return navigator.onLine? 'On line': 'Off line';
    },
    //Phonegap
    isNetworkConnected: function () {
        if (_hasConnectionType()) {
            return (_connectionType() !== Connection.NONE);
        } else { return navigator.onLine; } 
    },
    forecastImagesLocalBaseUrl: './images/sym/',
    //Phonegap
	getGPSPosition: function (onSuccess, onFail) {
	    navigator.geolocation.getCurrentPosition(
            function (position) {
            	onSuccess(position)
            }, 
            function (error) {
            	onFail(error);
            }, 
            { maximumAge: 60000, timeout: 5000, enableHighAccuracy: true });
		},
    //Phonegap
    vibrate: function () {
        if (!ns.settings.vibrate) return;
        if (typeof (navigator.notification) !== 'undefined') {
            navigator.notification.vibrate(40); 
        } 
    },
		
    logobj: function(obj) 
    {
        var a = [];
        a.push(' {');
        for (p in obj) if (_hasOwnProperty.call(obj, p)) { 
            a.push('    '+p+': '+obj[p]+',');
        }    
        a.push(' }');
        console.log(a.join('\n'));
    }
    // ,
    // logarray: function(a) 
    // {
        // console.log('[ ');
        // for (var i = 0, n = a.length; i < n; i++) 
          // console.log('    '+a[i]);
        // console.log(' ]');
    // }		
		  
  	};

	return ns;
	
})(myWF || {});