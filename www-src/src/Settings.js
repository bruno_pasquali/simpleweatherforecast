var myWF = (function(ns) {
  ns.settings = {
    temperatureMeasureUnit: 'C',
    windMeasureUnit: ns.Wind.KILOMETERS_PER_HOUR,
    vibrate: true,
    
    convertTemperature: function (t) { 
        return (ns.settings.temperatureMeasureUnit === 'F' ? Math.round((t*9/5)+32) : t);
    },
    save: function() {
      ns.storage.saveSettingsData(ns.settings);
      return ns.settings;
    },
    load: function() {
      ns.util.assingPropertyValues(ns.storage.loadSettingsData(), ns.settings);
      return ns.settings;
    }
  };

  return ns;
})(myWF || {});