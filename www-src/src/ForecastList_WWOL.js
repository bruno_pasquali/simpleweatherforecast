//http://www.worldweatheronline.com/free-weather-feed.aspx
//http://www.worldweatheronline.com/weather-api.aspx
var myWF = (function(ns) {

    var _util = ns.util;
        _imagesList = {
    "113": { imageDay: "32", imageNight: "31" },
    "116": { imageDay: "44", imageNight: "33" },
    "119": { imageDay: "28", imageNight: "29" },
    "122": { imageDay: "26", imageNight: "26" },
    "143": { imageDay: "211", imageNight: "211" },
    "176": { imageDay: "9", imageNight: "45" },
    "179": { imageDay: "5", imageNight: "5" },
    "182": { imageDay: "7", imageNight: "7" },
    "185": { imageDay: "5", imageNight: "5" },
    "200": { imageDay: "37", imageNight: "47" },
    "227": { imageDay: "16", imageNight: "16" },
    "230": { imageDay: "43", imageNight: "43" },
    "248": { imageDay: "201", imageNight: "201" },
    "260": { imageDay: "201", imageNight: "201" },
    "263": { imageDay: "9", imageNight: "45" },
    "266": { imageDay: "9", imageNight: "45" },
    "281": { imageDay: "5", imageNight: "5" },
    "284": { imageDay: "10", imageNight: "10" },
    "293": { imageDay: "8", imageNight: "8" },
    "296": { imageDay: "11", imageNight: "11" },
    "299": { imageDay: "132", imageNight: "45" },
    "302": { imageDay: "11", imageNight: "11" },
    "305": { imageDay: "312", imageNight: "45" },
    "308": { imageDay: "12", imageNight: "12" },
    "311": { imageDay: "5", imageNight: "5" },
    "314": { imageDay: "10", imageNight: "10" },
    "317": { imageDay: "7", imageNight: "7" },
    "320": { imageDay: "7", imageNight: "7" },
    "323": { imageDay: "134", imageNight: "46" },
    "326": { imageDay: "13", imageNight: "46" },
    "329": { imageDay: "14", imageNight: "14" },
    "332": { imageDay: "14", imageNight: "14" },
    "335": { imageDay: "134", imageNight: "46" },
    "338": { imageDay: "16", imageNight: "16" },
    "350": { imageDay: "6", imageNight: "6" },
    "353": { imageDay: "132", imageNight: "45" },
    "356": { imageDay: "11", imageNight: "11" },
    "359": { imageDay: "12", imageNight: "12" },
    "362": { imageDay: "10", imageNight: "10" },
    "365": { imageDay: "10", imageNight: "10" },
    "368": { imageDay: "13", imageNight: "13" },
    "371": { imageDay: "14", imageNight: "14" },
    "374": { imageDay: "6", imageNight: "6" },
    "377": { imageDay: "6", imageNight: "6" },
    "386": { imageDay: "37", imageNight: "47" },
    "389": { imageDay: "4", imageNight: "4" },
    "392": { imageDay: "37", imageNight: "47" },
    "395": { imageDay: "142", imageNight: "142" }
        };

    var convertImagePath = function(code, isDay) {
        if (_imagesList.hasOwnProperty(code)) {
            return (_util.forecastImagesLocalBaseUrl + _imagesList[code].imageDay + '.png');
        } 
        return (_util.forecastImagesLocalBaseUrl + 'na.png');
    } 

    var URLparts = [ '', '', '&format=json&includeLocation=yes&num_of_days=5&key='+ns.keys.WWOL ];
    
    var _getURL = function(objParameters) {
        if (ns.isLocal) {
            // URLparts[0] = 'http://localhost:8082/feed/weather.ashx?q=';
            URLparts[0] = 'http://localhost:8082/wwol/weather.ashx?q=';
        } else {
            // URLparts[0] = 'http://free.worldweatheronline.com/feed/weather.ashx?q=';
            URLparts[0] = 'http://api.worldweatheronline.com/free/v1/weather.ashx?q=';
        }        
        URLparts[1] = objParameters.query;
        return URLparts.join('');
    };

    var errorEmpty = function (error) {
        error.code = 'EmptyData';
        error.detail = '';
    };
    var errorError = function (error, errorData) {
        error.code = 'ErrorAnswer';
        error.detail = errorData["msg"] || "";
    };  
    var headOk = function(inData, error) {
        if ((! inData) || (! inData['data'])) { 
            errorEmpty(error); 
            return false; 
        }
        if (inData.data['error']) { 
            errorError(error, inData.data['error'][0] || ""); 
            return false; 
        }
        if ((!inData.data['current_condition']) ||
                (!inData.data.current_condition[0]) ||
                (!inData.data['weather']) ||
                (!inData.data.weather[0]) ||
                (!_util.isArray(inData.data['weather']))) {
            errorEmpty(error); 
            return false; 
        }
        return true; 
    };
    var convertDate = function(sDate) {
        return new Date(Date.UTC(parseInt(sDate.substr(0,4), 10), parseInt(sDate.substr(5,2), 10)-1, parseInt(sDate.substr(8,4), 10)));
    };
    var _convertHead = function(inData, outData, error) {
        if (! headOk(inData, error)) { 
            return []; 
        }
        // :todo gestire errore con unit test e query vuota
        var data = inData.data;
        var inCurrent = data.current_condition[0];
        var s = inCurrent["observation_time"] || "0"; //.toString(); // UTC hours and minutes
                                                        // AM/PM
        var pm = s.substr(6, 1) === 'P';
        var h = parseInt(s.substr(0, 2), 10);
        if (pm && (h !== 12)) { h = h+12; }
        if (!pm && (h === 12)) { h = 0; }
        var m = parseInt(s.substr(3, 2), 10);

        outData.currentConditions = _convertElement(inCurrent);
        var outCurrent = outData.currentConditions;
        outCurrent.date = convertDate((data.weather[0]["date"]) || "");

        var now = outCurrent.date;
        outCurrent.observationTime = new Date(Date.UTC(now.getFullYear(),
                now.getMonth(), now.getDate(), h, m));
        outCurrent.temperature = parseInt(inCurrent["temp_C"], 10) || 0;
        outCurrent.cloudCover = parseInt(inCurrent["cloudcover"], 10) || 0;
        outCurrent.visibility = parseInt(inCurrent["visibility"], 10) || 0;
        outCurrent.humidity = parseInt(inCurrent["humidity"], 10) || 0;
        outCurrent.pressure = parseInt(inCurrent["pressure"], 10) || 0;
        outCurrent.maxTemperature = parseInt(data.weather[0]["tempMaxC"], 10)  || 0;
        outCurrent.minTemperature = parseInt(data.weather[0]["tempMinC"], 10)  || 0;
        
        //data.weather.shift(); //remove element 0: data stored in .currentConditions
        return data.weather;
    };
    var _convertElement = function(e, error) {
        if (!e) { return false; }
        var f = new ns.Forecast();
        f.wind = new ns.Wind(e["winddir16Point"] || "", parseInt(e["windspeedKmph"], 10) || 0);
        f.precipitationMM = parseFloat(e["precipMM"]) || 0;
        f.date = convertDate(e["date"] || "");
        f.observationTime = f.date;
        f.maxTemperature = parseInt(e["tempMaxC"], 10)  || 0;
        f.minTemperature = parseInt(e["tempMinC"], 10)  || 0;
        f.customCode = e["weatherCode"] || "";
        if (e["weatherIconUrl"] && e.weatherIconUrl[0]) {
            f.iconUrl = convertImagePath(f.customCode, true) 
                || e.weatherIconUrl[0].value 
                || "";
            // f.iconUrl = convertImagePath(e.weatherIconUrl[0].value) || "";
        }
        if (e["weatherDesc"] && e.weatherDesc[0]) {
            f.customDescription = e.weatherDesc[0].value;   
            f.weatherDescription = f.customDescription;   
        }    
        return f;
    };
    ns.createForecastList_WorldWeatherOnLine = function() {
        _util.validateCreatorCall(this,
                ns.createForecastList_WorldWeatherOnLine);
        return {
            getURL : _getURL,
            convertHead : _convertHead,
            convertElement : _convertElement,
            imagesList: _imagesList
        };
    };

    return ns;
})(myWF || {});

