var myWF = (function (ns) {
	ns.Wind = function (direction, speed) {
		ns.util.validateConstructorCall(this, ns.Wind);

		var that = this;
		this.direction = direction || '';
		this.speed = speed || 0;
        this.getSpeed = function (mu) {
            switch (mu) {
                case ns.Wind.KNOTS: return Math.round(that.speed * 1.852); 
                case ns.Wind.MILES_PER_HOUR: return Math.round(that.speed * 0.621371192);
                case ns.Wind.METERS_PER_SECOND: return myWF.util.decRound(that.speed * 1000 / 3600, 1); 
                default: return that.speed; // km / h
            }
        };
	};
	ns.Wind.KNOTS = 'kn';
	ns.Wind.MILES_PER_HOUR = 'mph';
	ns.Wind.KILOMETERS_PER_HOUR = 'kmh';
	ns.Wind.METERS_PER_SECOND = 'ms';

	return ns;
}(myWF || {}));

/*
N	0°
NNE	22.5°
NE	45°
ENE	67.5°
E	90°
ESE	112.5°
SE	135°
SSE	157.5°
S	180°
SSW	202.5°
SW	225°
WSW	247.5°
W	270°
WNW	292.5°
NW	315°
NNW	337.5°

grado	velocità (km/h)	tipo di vento	(nodi)	condizioni ambientali	velocità (m/s)
0	0-1		calma	0-1	il fumo ascende verticalmente; il mare è uno specchio.	< 0.3
1	1-5		bava di vento	1-3	il vento devia il fumo; increspature dell'acqua.	0.3-1.5
2	6-11	brezza leggera	4-6	le foglie si muovono; onde piccole ma evidenti.	1.6-3.3
3	12-19	brezza	7-10	foglie e rametti costantemente agitati; piccole onde, creste che cominciano ad infrangersi.	3.4-5.4
4	20-28	brezza vivace	11-16	il vento solleva polvere,foglie secche,i rami sono agitati; piccole onde che diventano più lunghe.	5.5-7.9
5	29-38	brezza tesa	17-21	oscillano gli arbusti con foglie; si formano piccole onde nelle acque interne; onde moderate allungate.	8-10.7
6	39-49	vento fresco	22-27	grandi rami agitati, sibili tra i fili telegrafici; si formano marosi con creste di schiuma bianca, e spruzzi.	10.8-13.8
7	50-61	vento forte	28-33	interi alberi agitati, difficoltà a camminare contro vento; il mare è grosso, la schiuma comincia ad essere sfilacciata in scie.	13.9-17.1
8	62-74	burrasca moderata	34-40	rami spezzati, camminare contro vento è impossibile; marosi di altezza media e più allungati, dalle creste si distaccano turbini di spruzzi.	17.2-20.7
9	75-88	burrasca forte	41-47	camini e tegole asportati; grosse ondate, spesse scie di schiuma e spruzzi, sollevate dal vento, riducono la visibilità.	20.8-24.4
10	89-102	tempesta	48-55	rara in terraferma, alberi sradicati, gravi danni alle abitazioni; enormi ondate con lunghe creste a pennacchio.	24.5-28.4
11	103-117	fortunale	56-63	raro, gravissime devastazioni; onde enormi ed alte, che possono nascondere navi di media stazza; ridotta visibilità.	28.5-32.6
12	oltre 118	
*/