var myWF = (function (ns) {

	var store = window.localStorage,
		storage = {
			save: function (id, obj) {
				var j = ns.util.toJSON(obj);
				store.setItem(id.toString(), j);
				return j;
			},
			load: function (id) {
			    try {
			        return ns.util.parseJSON(store.getItem(id.toString()));
			    } catch (e) {
			        ns.util.logSet('Storage', e.toString())
                    if (ns.isDebug) throw e;
			        return null;
			    }
			},
			clearKey: function (key) {
				store.removeItem(key);
			}
		},
		ids = {
			locationDataUUID: "{7070EBAE-C7D2-4F1E-9664-5B2909126F12}",
			forecastDataUUID: "{B6C6344E-8B83-4798-9539-66ACD2464C9D}",
			settingsDataUUID: "{16CCC97A-F905-4BE6-9ACA-1AD84EDE7E19}"
		};

	ns.createStorage = function () {
		return {
			save: storage.save,
			load: storage.load,
			saveLocationData: function (data) {
				return storage.save(ids.locationDataUUID, data);
			},
			saveForecastData: function (data) {
				return storage.save(ids.forecastDataUUID, data);
			},
			saveSettingsData: function (data) {
				return storage.save(ids.settingsDataUUID, data);
			},
			loadLocationData: function () {
				return storage.load(ids.locationDataUUID);
			},
			loadForecastData: function () {
				return storage.load(ids.forecastDataUUID);
			},
			loadSettingsData: function () {
				return storage.load(ids.settingsDataUUID);
			},
			clear: function () {
				for (var k in ids) {
					if (ids.hasOwnProperty(k)) {
						storage.clearKey(k);
					}
				}
			}
		};
	};

	ns.storage = ns.createStorage();

	return ns;
})(myWF || {});

