//see: 
//http://developer.yahoo.com/geo/placefinder/
//http://code.google.com/apis/maps/documentation/geocoding/#ReverseGeocoding
/*
 * 
Code	 Description
400	 Bad request. The parameters passed to the service did not match as expected. The Message should tell you what was missing or incorrect.
403	 Forbidden. You do not have permission to access this resource, or are over your rate limit.
503	 Service unavailable. An internal problem prevented us from returning data to you.
 */
var myWF = (function(ns) {
    var URLparts = [ '', '', '',
            ' and locale="', '', '" &format=json ' ];
//old            '&locale=', '', '&flags=J&appid='+ns.keys.YAHOO_appid ];
//http://query.yahooapis.com/v1/public/yql?q=select * from geo.placefinder where text="london, usa" and gflags="R" &format=json
//old            URLparts[0] = 'http://where.yahooapis.com/geocode?location=';
    var _byLatLong = function (objParameters) {
        var o = objParameters;
        return '"' + o.latitude + ',' + o.longitude + '" and gflags="R" ';
    };
    var _byAddressAndCountry = function (objParameters) {
        var o = objParameters;
        return '"' + o.address + (o.country ? (',' + o.country) : '') + '" ';
    };
    var _getURL = function(queryObject) {
	    if (ns.isLocal) {						 
	        URLparts[0] = 'http://localhost:8082';
	    } else {
	           URLparts[0] = 'http://query.yahooapis.com/v1/public';
	    }
        URLparts[1] = '/yql/yql?q=select * from geo.placefinder where text=';
	    var by = queryObject['by'] || _byAddressAndCountry;
        URLparts[2] = queryObject.by(queryObject);
        URLparts[4] = ns.util.getLocale();
        return URLparts.join('');
    };
    var _convertHead = function(inData, outData, error) {
        // if ((!inData) || (!inData['ResultSet'])
                // || (!inData.ResultSet['Results'])
                // || (!ns.util.isArray(inData.ResultSet.Results))) {
        if ((!inData) || (!inData['query'])
                || (!inData.query['results'])
                || (!inData.query.results['Result'])) {
            error.code = 'EmptyData';
            return [];
        }
        var r = inData.query.results.Result;
        return (ns.util.isArray(r) ? r : [r]);
    };

    var _convertElement = function(element) {
        if (!element) { return false; }
        var location = new ns.Location(); //
        location.latitude = element["latitude"] || 0;
        location.longitude = element["longitude"] || 0;
        location.city = element["city"] || "";
        location.state = element["state"] || "";
        location.country = element["country"] || "";
        location.county = element["county"] || "";
        location.countryCode = element["countrycode"] || "";
        location.countyCode = element["countycode"] || "";
        location.neighborhood = element["neighborhood"] || "";
        return location;
    };
    
    ns.createLocationList_YAHOO = function() {
        ns.util.validateCreatorCall(this, ns.createLocationList_YAHOO);
        return {
            getURL : _getURL,
            convertHead : _convertHead,
            convertElement : _convertElement,
            
            byLatLong: _byLatLong,
            byAddressAndCountry: _byAddressAndCountry
        };
    };
    
    return ns;
})(myWF || {});
