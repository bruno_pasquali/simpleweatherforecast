var myWF = (function(ns) {
    
    var setStatusOK = function (status) {
        status.code = 'ok';
        status.detail = '';
    };
    var setStatusUpdating = function (status) {
        status.code = 'updating';
        status.detail = '';
    };
    var setStatusTimeout = function (status) {
        status.code = 'timeout';
        status.detail = '';
    };
    var convertData = function (that) {
        var service = that.serviceAdapter,
            status = that.status;
            
        setStatusOK(status);
        that.data = {};
        that.data.list = [];
        gotData = that.rawData;
        var list = that.data.list;
        try {
            ns.util.iterate(service.convertHead(gotData, that.data, status),
                function(e) {
                    var element = service.convertElement(e);
                    if (element) { list.push(element); }
                });
        }
        catch (e) {
            status.code = 'DataConversionError';
            status.detail = e.toString();
            ns.util.logSet('Ajax data conversion error', status.detail)
            if (ns.isDebug) throw e;
        }
        return status.isOk();
    };

    var _update = function(that, objParameters) {
        var status = that.status;
        //var options = (typeof objOptions) === 'undefined' ? {}: objOptions;
        setStatusUpdating(status);
        ns.util.ajax(that.serviceAdapter.getURL(objParameters), 
            function(gotData, ajaxStatus, objParameters) {
                that.rawData = gotData;
                status.detail = ajaxStatus.toString();
                if (convertData(that)) {
                    that.onSuccess(objParameters);
                } else {
                    that.onFail(objParameters);
                }        
            }, 
            function(exception, ajaxStatus, objParameters) {
                if (ajaxStatus === 'Timeout') {
                    setStatusTimeout(status);
                } else {    
                    status.code = 'RequestFailed';
                    status.detail = ajaxStatus + ', ' + exception.toString();
                }    
                that.onFail();
            },
            objParameters
        );
    };

    
    ns.createGeoAjaxList = function(serviceAdapter, inStorage) {
        ns.util.validateCreatorCall(this, ns.createGeoAjaxList);

        var that = {};
        var storage = inStorage || {
            save: function () { }, 
            load: function () { return that.data; }
        };

        that.serviceAdapter = serviceAdapter;
        that.data = {};
        that.rawData = null;
        that.onSuccess = function () {};
        that.onFail = function () {};
        that.status = {
            code: '',
            detail: '',
            emptyData: function () { return (that.status.code === 'EmptyData'); },
            isTimeout: function () { return (that.status.code === 'timeout'); },
            isOk: function () { return (that.status.code === 'ok'); },
            isUpdating: function () { return (that.status.code === 'updating'); },
            isNotOk: function () { return !(that.status.isOk() || that.status.isUpdating()); }
        };
        that.update = function(objParameters) {
            _update(that, objParameters);
        };
        that.save = function() {
            storage.save(that.rawData);
        };
        that.load = function() {
            that.rawData = storage.load();
            convertData(that);
            return that.data;
        };

        return that;
    };

    return ns;
})(myWF || {});
