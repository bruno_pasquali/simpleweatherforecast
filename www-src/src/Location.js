var myWF = (function(ns) {
	
    var trim = myWF.util.trim, 
    pushField = function (stringField, array) {
        if (!stringField) return;
        var t = trim(stringField);
        if (t === '') return;
        for (var i=0; i<array.length; i++) {
            if (array[i] == t) return;
        }
        array.push(t); 
    };
    var _getString = function (that) {
        var array = [];
        pushField(that.city, array);
        //pushField(that.countyCode, array);
        pushField(that.state, array);
        pushField(that.country, array);
        return array.join(', ');   
    };
	var _getLongString = function (that) {
        var array = [];
        pushField(that.neighborhood, array);
        pushField(that.city, array);
        pushField(that.countyCode, array);
        pushField(that.county, array);
        pushField(that.state, array);
        pushField(that.country, array);
        return array.join(', ');   
    };
	var _getGeoLocationString = function (that) {
		if (!that.latitude) return '';
        return that.latitude.toString() + ',' + that.longitude.toString();
    };
	var _identity = function (that) {
        return (that.city + that.state + that.country) ||
            ((that.latitude.toString() || '') + (that.longitude.toString() || ''));
	};
    
	
    ns.Location = function () {
        ns.util.validateConstructorCall(this, ns.Location);
        
        // :todo aggiungere test per tutte le props
        var that = this;
        
        this.latitude = null;
        this.longitude = null;
    
        this.neighborhood = '';
        this.city = '';
        this.county = '';
        this.state = '';
        this.country = '';
        this.countryCode = '';
        this.countyCode = '';
        this.lastUpdateTime = new Date().getTime();
        this.identity = function () { return _identity(that); };
        this.getString = function () { return _getString(that); };
        this.getGeoLocationString = function () { return _getGeoLocationString(that); };
        this.getLongString = function () { return _getLongString(that); };
    };

	return ns;
	
})(myWF || {});
