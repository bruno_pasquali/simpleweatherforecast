var myWF = (function (ns) {

    var sortFunction = function (a, b) {
            var sa = a.identity(),
				sb = b.identity();
            return ((sa > sb ? 1 : (sa < sb ? -1 : 0)));
        };
		
	ns.createItemsBag = function (storage) {
        var items = [];
		var itemsBag = {
            currentItem: null,
            previousItem: null,
			sort: function () {
				items.sort(sortFunction);
				return itemsBag;
			},
			getByID: function (identity) {
				return items[itemsBag.identityIndex(identity)];
			},
			getByIndex: function (index) {
				return (items.length>index? items[index]: null);
			},
			toArray: function () {
				itemsBag.sort();
				return items.slice();
			},
			length: function () {
				return items.length;
			},
			itemIndex: function (item) {
				var id = item.identity(), pos = -1, i = 0;

				for (i = 0; i < itemsBag.length(); i = i + 1) {
					if (itemsBag.getByIndex(i).identity() === id) {
						pos = i;
						break;
					}
				}
				return pos;
			},
			identityIndex: function (identity) {
				var pos = -1, i = 0;
				for (i = 0; i < itemsBag.length(); i = i + 1) {
					if (itemsBag.getByIndex(i).identity() === identity) {
						pos = i;
						break;
					}
				}
				return pos;
			},
			addUnsorted: function (item) {
				if (((typeof item) === 'undefined') || (item === null)) {
					return itemsBag;
				}
				var pos = itemsBag.itemIndex(item);
				if (pos !== -1) {
					items.splice(pos, 1, item);
				} else {
					items.push(item);
				}
				return itemsBag;
			},
			add: function (item) {
				return (itemsBag.addUnsorted(item)).sort();
			},
			empty: function () {
				items = [];
				return itemsBag;
			},
			remove: function (item) {
				var pos = itemsBag.itemIndex(item);
				if (pos !== -1) {
					items.splice(pos, 1);
				}
				return itemsBag;
			},
			save: function () {
				var bag = {
                    current: itemsBag.currentItem,
                    previous: itemsBag.previousItem,
					items: items
				};
				storage.save(bag);
				return itemsBag;
			},
			load: function () {
				var bag = storage.load() || {},
					jsonItems = bag["items"] || [],
                    jsonCurrent = bag["current"] || null,
                    jsonPrevious = bag["previous"] || null,
					item;
				itemsBag.currentItem = null;
                itemsBag.previousItem = null;
				if (jsonCurrent) {
					item = new storage.ItemConstructor();
					ns.util.assingPropertyValues(jsonCurrent, item);
					itemsBag.currentItem = item;
				}
                if (jsonPrevious) {
                    item = new storage.ItemConstructor();
                    ns.util.assingPropertyValues(jsonPrevious, item);
                    itemsBag.previousItem = item;
                }
				if (!ns.util.isArray(jsonItems)) {
					items = [];
					return itemsBag;
				}
				items = ns.util.map(jsonItems, function (jsonItem) {
					var item = new storage.ItemConstructor();
					ns.util.assingPropertyValues(jsonItem, item);
					return item;
				});

				return itemsBag;
			}
		};

		return itemsBag;
	};

    return ns;
}(myWF || {}));

/*
xMyLocations = {
    currentLocationIndex: -1,
	items: {},
	storage: myWF.storage,
	sortFunction: function(a, b) {
		var sa = a.identity();
		var sb = b.identity();
		return ((sa > sb ? 1 : (sa < sb ? -1 : 0)));
	},
	toArray: function() {
		var a = [];
		for (var p in MyLocations.items) {
			if (MyLocations.items.hasOwnProperty(p)) {
				a.push(MyLocations.items[p]);
			}
		}
		a.sort(MyLocations.sortFunction);
		return a;
	},
	add: function(item) {
		MyLocations.items[item.identity()] = item;
		return MyLocations;
	},
	empty: function() {
		MyLocations.items = {};
		return MyLocations;
	},
	remove: function(item) {
		delete MyLocations.items[item.identity()];
	},
	save: function() {
		var j = MyLocations.storage.saveLocationData(MyLocations.items);
		return MyLocations;
	},
	load: function() {
		var jsonItems = MyLocations.storage.loadLocationData() || {};
		MyLocations.items = {};
		for (var jl in jsonItems) {
			var l = new myWF.Location();
			for (var p in l) {
				if (jl[p]) {
					l[p] = jl[p];
				}
			}
			MyLocations.items[l.identity()] = l;
		};
		return MyLocations;
	}
};
*/