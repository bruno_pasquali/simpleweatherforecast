var myWF = (function(ns) {

    ns.bootstrap = function () {
        ns.myLocations = ns.createItemsBag({
            load: ns.storage.loadLocationData,
            save: ns.storage.saveLocationData,
            ItemConstructor: ns.Location
        });        
        ns.settings.load();
        ns.myLocations.load();
        ns.forecastList = myWF.createGeoAjaxList(
                myWF.createForecastList_WorldWeatherOnLine(),
                {
                    load: ns.storage.loadForecastData,
                    save: ns.storage.saveForecastData
                }
        );
        ns.forecastList.load();
        ns.locationList = ns.createGeoAjaxList(ns.createLocationList_YAHOO());
    };
    
    ns.bootstrap();
    
    return ns;
})(myWF || {});