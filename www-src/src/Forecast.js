var myWF = (function(ns) {
	ns.Forecast = function() {
		//:todo definire un oggetto "todayForecast"
		ns.util.validateConstructorCall(this, ns.Forecast);
		//:todo doc con specifiche su u.m., conversioni fatte, data di quando (:observationDate)? 
        this.date = new Date();
        this.observationTime = this.date;
		this.wind = new ns.Wind();

		this.temperature = 0;
		this.humidity = 0;
		this.pressure = 0;
		this.cloudCover = 0;
		this.visibility = 0;

		this.maxTemperature = 0;
		this.minTemperature = 0;
		this.precipitationMM = 0.0;
		this.weatherDescription = '';
		this.iconUrl = '';
		this.customCode = '';
		this.customDescription = '';
	};

	return ns;
})(myWF || {});