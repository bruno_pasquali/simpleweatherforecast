#!/usr/local/bin/ruby -w
require 'erb'

class HtmlGenerator
  def initialize(erb_file)
    @erb = ERB.new File.new(erb_file).read, nil, "%"
  end
  def run(sources, css, local, debug, fileName)
    @localRequests = local
    @debug = debug
    @sources = sources 
    @css = css 
    File.open(fileName, 'w') do |f|
      f.write @erb.result(binding)
    end
  end
end
